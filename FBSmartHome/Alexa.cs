﻿namespace FBSmartHome
{
    using RKon.Alexa.NET46.JsonObjects;
    using RKon.Alexa.NET46.Payloads;
    using RKon.Alexa.NET46.Request;
    using RKon.Alexa.NET46.Response;
    using RKon.Alexa.NET46.Types;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public sealed class Alexa
    {
        public static readonly Alexa Instance = new Alexa();
        private readonly IFritzBox FritzBoxInstance;

        static Alexa()
        {
        }

        private Alexa()
        {
            if (Environment.GetEnvironmentVariable("USE_SNS") == "true")
            {
                FritzBoxInstance = FritzBoxViaRabbitMQ.Instance;
            }
            else
            {
                FritzBoxInstance = FritzBox.Instance;
            }
        }

        // Handle Alexa Grant Directives
        // Exchange code for access token and save customer ID
        public async Task<SmartHomeResponse> HandleAcceptGrantDirectiveAsync(SmartHomeRequest request)
        {
            SmartHomeResponse response = new SmartHomeResponse(request.Directive.Header);

            string code = ((AcceptGrantRequestPayload)request.Directive.Payload).Grant.Code;
            string grantee = ((AcceptGrantRequestPayload)request.Directive.Payload).Grantee.Token;

            bool exchangeSuccess = await OAuth.Instance.ExchangeCodeForTokenAsync(grantee, code).ConfigureAwait(false);

            if (!exchangeSuccess)
            {
                Logger.Instance.Debug("Token exchange failed");

                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ACCEPT_GRANT_FAILED);
                ((ErrorPayload)response.Event.Payload).Message = "Access could not be granted.";
            }

            return response;
        }

        // Handle Alexa Discovery Directives
        // Reply with a list of found devices
        public SmartHomeResponse HandleDiscoveryDirective(SmartHomeRequest request, Customer customer)
        {
            SmartHomeResponse response = new SmartHomeResponse(request.Directive.Header);

            foreach (Device device in customer.DeviceList)
            {
                ResponseEndpoint endpoint = new ResponseEndpoint
                {
                    EndpointID = device.Identifier,
                    ManufacturerName = device.Manufacturer,
                    Description = device.ProductName,
                    FriendlyName = device.Name
                };

                endpoint.AdditionalAttributes = new AdditionalAttributes
                {
                    Manufacturer = device.Manufacturer,
                    Model = device.ProductName,
                    CustomIdentifier = device.Identifier
                };

                endpoint.AdditionalAttributes.FirmwareVersion = device.Firmware.Length > 0 ? device.Firmware : "0";

                endpoint.Connections = new List<EndpointConnection>
                {
                    new EndpointConnection(ConnectionType.UNKNOWN, "DECT")
                };
                
                endpoint.Cookies = new Dictionary<string, string>
                {
                    { "customerId", customer.CustomerId }
                };

                List<string> propertyNames;
                Capability capability;

                if (device.IsDeviceClass(DeviceClass.Thermostat))
                {
                    endpoint.DisplayCategories.Add(DisplayCategory.THERMOSTAT);

                    propertyNames = new List<string>
                    {
                        PropertyNames.TARGET_SETPOINT,
                        PropertyNames.THERMOSTATMODE
                    };
                    capability = new Capability(Namespaces.ALEXA_THERMOSTATCONTROLLER, "3", propertyNames, false, true)
                    {
                        Configuration = new Configuration
                        {
                            SupportsScheduling = false,
                            SupportedModes = new List<ThermostatModes>
                            {
                                ThermostatModes.HEAT,
                                ThermostatModes.ECO,
                                ThermostatModes.OFF
                            }
                        }
                    };

                    endpoint.Capabilities.Add(capability);

                    propertyNames = new List<string>
                    {
                        PropertyNames.POWER_STATE
                    };
                    capability = new Capability(Namespaces.ALEXA_POWERCONTROLLER, "3", propertyNames, false, true);
                    endpoint.Capabilities.Add(capability);
                }

                if (device.IsDeviceClass(DeviceClass.PowerSwitch))
                {
                    endpoint.DisplayCategories.Add(DisplayCategory.SMARTPLUG);

                    propertyNames = new List<string>
                    {
                        PropertyNames.POWER_STATE,
                    };
                    capability = new Capability(Namespaces.ALEXA_POWERCONTROLLER, "3", propertyNames, false, true);
                    endpoint.Capabilities.Add(capability);
                }

                if (device.IsDeviceClass(DeviceClass.TemperatureSensor)
                    || (device.IsDeviceClass(DeviceClass.Thermostat) && device.IsGroup))
                {
                    endpoint.DisplayCategories.Add(DisplayCategory.TEMPERATURE_SENSOR);

                    propertyNames = new List<string>
                    {
                        PropertyNames.TEMPERATURE,
                    };
                    capability = new Capability(Namespaces.ALEXA_TEMPERATURESENSOR, "3", propertyNames, false, true);
                    endpoint.Capabilities.Add(capability);
                }

                if (device.IsDeviceClass(DeviceClass.PowerSwitch)
                    || device.IsDeviceClass(DeviceClass.TemperatureSensor)
                    || device.IsDeviceClass(DeviceClass.Thermostat))
                {
                    propertyNames = new List<string>
                    {
                        PropertyNames.CONNECTIVITY,
                    };
                    capability = new Capability(Namespaces.ALEXA_ENDPOINTHEALTH, "3", propertyNames, false, true);
                    endpoint.Capabilities.Add(capability);

                    ((DiscoveryPayload)response.Event.Payload)?.Endpoints.Add(endpoint);
                }

                if (device.IsDeviceClass(DeviceClass.Template))
                {
                    endpoint.DisplayCategories.Add(DisplayCategory.SCENE_TRIGGER);

                    capability = new Capability(Namespaces.ALEXA_SCENECONTROLLER, "3", false);
                    endpoint.Capabilities.Add(capability);

                    ((DiscoveryPayload)response.Event.Payload)?.Endpoints.Add(endpoint);
                }


                // If device needs an additional switch, create a powercontroller for it
                if (device.IsDeviceClass(DeviceClass.Thermostat) && device.AdditionalSwitch)
                {
                    endpoint = new ResponseEndpoint
                    {
                        EndpointID = device.Identifier + "-virtual",
                        ManufacturerName = device.Manufacturer,
                        FriendlyName = device.Name + " S",
                        Description = device.ProductName + " S"
                    };
                    
                    endpoint.AdditionalAttributes = new AdditionalAttributes
                    {
                        Manufacturer = device.Manufacturer,
                        Model = device.ProductName,
                        CustomIdentifier = device.Identifier + "-virtual"
                    };

                    endpoint.AdditionalAttributes.FirmwareVersion = device.Firmware.Length > 0 ? device.Firmware : "0";

                    endpoint.Connections = new List<EndpointConnection>
                    {
                        new EndpointConnection(ConnectionType.UNKNOWN, "VIRTUAL")
                    };
                    
                    endpoint.Cookies = new Dictionary<string, string>
                    {
                        { "customerId", customer.CustomerId }
                    };

                    endpoint.DisplayCategories.Add(DisplayCategory.SMARTPLUG);

                    propertyNames = new List<string>
                    {
                        PropertyNames.POWER_STATE,
                    };
                    capability = new Capability(Namespaces.ALEXA_POWERCONTROLLER, "3", propertyNames, false, false);
                    endpoint.Capabilities.Add(capability);

                    ((DiscoveryPayload)response.Event.Payload)?.Endpoints.Add(endpoint);
                }
            }

            return response;
        }

        // Handle Alexa Deferred Report State
        public SmartHomeResponse HandleDeferredReportStateDirective(SmartHomeRequest request)
        {
            SmartHomeResponse response = SmartHomeResponse.CreateDeferredResponse(request.Directive.Header);
            ((DeferredResponsePayload)response.Event.Payload).EstimatedDeferralInSeconds = 7;

            return response;
        }

        // Handle Alexa Report State Directive
        // Answer with a State Report for the given device
        public SmartHomeResponse HandleReportStateDirective(SmartHomeRequest request, Customer customer, Device device)
        {
            SmartHomeResponse response = new SmartHomeResponse(request.Directive.Header);
            response.Event.Endpoint = new Endpoint()
            {
                EndpointID = device.Identifier
            };

            if (device.IsDeviceClass(DeviceClass.PowerSwitch))
            {
                Context context = new Context();

                PowerStates powerState = device.SwitchedOn ? PowerStates.ON : PowerStates.OFF;
                context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 0));

                if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
                {
                    Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                    context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 0));
                }

                ConnectivityModes connectivity = device.Present ? ConnectivityModes.OK : ConnectivityModes.UNREACHABLE;
                context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, connectivity, DateTime.UtcNow, 0));

                response.Context = context;
            }
            else if (device.IsDeviceClass(DeviceClass.Thermostat))
            {
                Context context = new Context();

                // App crashes if no Temperature response is sent for thermostat?!?
                // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                Setpoint temperature;
                if (device.IsGroup)
                {
                    temperature = new Setpoint(device.TemperatureActualValue, Scale.CELSIUS);
                    if (device.GroupMembers.Count > 0)
                    {
                        Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                        if (firstGroupDevice != null)
                        {
                            temperature = new Setpoint(firstGroupDevice.Temperature, Scale.CELSIUS);
                        }
                    }
                }
                else
                {
                    temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                }

                context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                double temperatureSetpoint;
                if (device.TemperatureSetpoint == (double)Temperature.ON)
                {
                    temperatureSetpoint = 28;
                }
                else if (device.TemperatureSetpoint == (double)Temperature.OFF)
                {
                    temperatureSetpoint = 8;
                }
                else
                {
                    temperatureSetpoint = device.TemperatureSetpoint;
                }

                Setpoint setPointTemperature = new Setpoint(temperatureSetpoint, Scale.CELSIUS);
                context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setPointTemperature, DateTime.UtcNow, 900000));

                ThermostatModes thermostatMode;
                if (device.TemperatureSetpoint == (double)Temperature.OFF)
                {
                    thermostatMode = ThermostatModes.OFF;
                }
                else if (device.TemperatureSetpoint <= device.TemperatureSetback
                    && device.TemperatureSetback < device.TemperatureComfort
                    && device.TemperatureSetback != (double)Temperature.OFF
                    && device.TemperatureSetback != (double)Temperature.ON)
                {
                    thermostatMode = ThermostatModes.ECO;
                }
                else
                {
                    thermostatMode = ThermostatModes.HEAT;
                }

                context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, thermostatMode, DateTime.UtcNow, 900000));

                PowerStates powerState;
                if (device.TemperatureSetpoint == (double)Temperature.OFF || device.TemperatureSetpoint == device.TemperatureOff)
                {
                    powerState = PowerStates.OFF;
                }
                else
                {
                    powerState = PowerStates.ON;
                }

                context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                ConnectivityModes connectivity = device.Present ? ConnectivityModes.OK : ConnectivityModes.UNREACHABLE;
                context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, connectivity, DateTime.UtcNow, 900000));

                response.Context = context;
            }
            else if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
            {
                Context context = new Context();

                Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                ConnectivityModes connectivity = device.Present ? ConnectivityModes.OK : ConnectivityModes.UNREACHABLE;
                context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, connectivity, DateTime.UtcNow, 0));

                response.Context = context;
            }

            return response;
        }

        // Handle Alexa PowerController Directive
        // Set Smart Plug to the asked state if possible
        public async Task<SmartHomeResponse> HandlePowerControllerDirectiveAsync(SmartHomeRequest request, Device device, Customer customer)
        {
            SmartHomeResponse response;

            if (request.Directive.Header.Name == HeaderNames.TURN_ON_REQUEST)
            {
                ConnectionState connectionState;

                double onTemperature = device.TemperatureComfort;

                // If device is a thermostat, hand over to thermostat control
                if (device.IsDeviceClass(DeviceClass.Thermostat))
                {
                    // check for user set temperature. If not set, use comfort temperature
                    if ((device.TemperatureOn >= (double)Temperature.MINIMUM_VALUE && device.TemperatureOn <= (double)Temperature.MAXIMUM_VALUE)
                        || (device.TemperatureOn >= (double)Temperature.NOCHANGE && device.TemperatureOn <= (double)Temperature.COMFORT))
                    {
                        if (device.TemperatureOn == (double)Temperature.NOCHANGE)
                        {
                            onTemperature = device.TemperatureSetpoint;
                        }
                        else if (device.TemperatureOn == (double)Temperature.ECO)
                        {
                            onTemperature = device.TemperatureSetback;
                        }
                        else if (device.TemperatureOn == (double)Temperature.COMFORT)
                        {
                            onTemperature = device.TemperatureComfort;
                        }
                        else
                        {
                            onTemperature = device.TemperatureOn;
                        }
                    }

                    // If no change is requested, just update devices
                    if (device.TemperatureOn == (double)Temperature.NOCHANGE)
                    {
                        connectionState = await FritzBoxInstance.GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), true).ConfigureAwait(false);
                    }
                    else
                    {
                        connectionState = await FritzBoxInstance.HandleSetTemperatureAsync(device, onTemperature, customer).ConfigureAwait(false);
                    }
                }
                else
                {
                    connectionState = await FritzBoxInstance.HandleTurnOnAsync(device, customer).ConfigureAwait(false);
                }

                if (connectionState == ConnectionState.OK)
                {
                    response = new SmartHomeResponse(request.Directive.Header);
                    response.Event.Endpoint = new Endpoint()
                    {
                        EndpointID = device.Identifier
                    };
                    Context context = new Context();

                    context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, PowerStates.ON, DateTime.UtcNow, 0));

                    if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
                    {
                        Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 0));
                    }

                    if (device.IsDeviceClass(DeviceClass.Thermostat))
                    {
                        double temperatureSetpoint;
                        if (onTemperature == (double)Temperature.ON)
                        {
                            temperatureSetpoint = 28;
                        }
                        else if (onTemperature == (double)Temperature.OFF)
                        {
                            temperatureSetpoint = 8;
                        }
                        else
                        {
                            temperatureSetpoint = onTemperature;
                        }

                        Setpoint setpoint = new Setpoint(temperatureSetpoint, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));

                        ThermostatModes thermostatMode;
                        if (onTemperature == (double)Temperature.OFF)
                        {
                            thermostatMode = ThermostatModes.OFF;
                        }
                        else if (onTemperature <= device.TemperatureSetback && device.TemperatureSetback < device.TemperatureComfort)
                        {
                            thermostatMode = ThermostatModes.ECO;
                        }
                        else
                        {
                            thermostatMode = ThermostatModes.HEAT;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, thermostatMode, DateTime.UtcNow, 900000));
                    }

                    context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 0));

                    response.Context = context;
                }
                else if (connectionState == ConnectionState.FAILED)
                {
                    // Endpoint is unreachable
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Smartplug could not be switched on";
                }
                else
                {
                    // Connection failed
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Smartplug could not be switched on";
                }
            }
            else if (request.Directive.Header.Name == HeaderNames.TURN_OFF_REQUEST)
            {
                ConnectionState connection;

                double offTemperature = (double)Temperature.OFF;

                // If device is a thermostat, hand over to thermostat control
                if (device.IsDeviceClass(DeviceClass.Thermostat))
                {
                    // check for user set temperature. If not set, set thermostat off
                    if ((device.TemperatureOff >= (double)Temperature.MINIMUM_VALUE && device.TemperatureOff <= (double)Temperature.MAXIMUM_VALUE)
                        || (device.TemperatureOff >= (double)Temperature.NOCHANGE && device.TemperatureOff <= (double)Temperature.COMFORT))
                    {
                        if (device.TemperatureOff == (double)Temperature.NOCHANGE)
                        {
                            offTemperature = device.TemperatureSetpoint;
                        }
                        else if (device.TemperatureOff == (double)Temperature.ECO)
                        {
                            offTemperature = device.TemperatureSetback;
                        }
                        else if (device.TemperatureOff == (double)Temperature.COMFORT)
                        {
                            offTemperature = device.TemperatureComfort;
                        }
                        else
                        {
                            offTemperature = device.TemperatureOff;
                        }
                    }

                    // If no change is requested, just update devices
                    if (device.TemperatureOff == (double)Temperature.NOCHANGE)
                    {
                        connection = await FritzBoxInstance.GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), true).ConfigureAwait(false);
                    }
                    else
                    {
                        connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, offTemperature, customer).ConfigureAwait(false);
                    }
                }
                else
                {
                    connection = await FritzBoxInstance.HandleTurnOffAsync(device, customer).ConfigureAwait(false);
                }

                if (connection == ConnectionState.OK)
                {
                    response = new SmartHomeResponse(request.Directive.Header);
                    response.Event.Endpoint = new Endpoint()
                    {
                        EndpointID = device.Identifier
                    };
                    Context context = new Context();

                    context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, PowerStates.OFF, DateTime.UtcNow, 0));

                    if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
                    {
                        Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 0));
                    }

                    if (device.IsDeviceClass(DeviceClass.Thermostat))
                    {
                        double temperatureSetpoint;
                        if (offTemperature == (double)Temperature.ON)
                        {
                            temperatureSetpoint = 28;
                        }
                        else if (offTemperature == (double)Temperature.OFF)
                        {
                            temperatureSetpoint = 8;
                        }
                        else
                        {
                            temperatureSetpoint = offTemperature;
                        }

                        Setpoint setpoint = new Setpoint(temperatureSetpoint, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));

                        ThermostatModes thermostatMode;
                        if (offTemperature == (double)Temperature.OFF)
                        {
                            thermostatMode = ThermostatModes.OFF;
                        }
                        else if (offTemperature <= device.TemperatureSetback && device.TemperatureSetback < device.TemperatureComfort)
                        {
                            thermostatMode = ThermostatModes.ECO;
                        }
                        else
                        {
                            thermostatMode = ThermostatModes.HEAT;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, thermostatMode, DateTime.UtcNow, 900000));
                    }

                    context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 0));

                    response.Context = context;
                }
                else if (connection == ConnectionState.FAILED)
                {
                    // Endpoint is unreachable
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Smartplug could not be switched off";
                }
                else
                {
                    // Connection failed
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Smartplug could not be switched off";
                }
            }
            else
            {
                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.INVALID_DIRECTIVE);
                ((ErrorPayload)response.Event.Payload).Message = "Smartplug does not know this directive";
            }

            return response;
        }

        // Handle Alexa Thermostat Directive
        // Set Thermostat to the asked value if possible
        public async Task<SmartHomeResponse> HandleThermostatDirectiveAsync(SmartHomeRequest request, Device device, Customer customer)
        {
            SmartHomeResponse response;

            if (request.Directive.Header.Name == HeaderNames.SETTARGETTEMPERATURE)
            {
                SetTargetTemperatureRequestPayload payload = (SetTargetTemperatureRequestPayload)request.Directive.Payload;
                if (payload.TargetSetpoint == null && payload.LowerSetpoint != null && payload.UpperSetpoint != null)
                {
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.DUAL_SETPOINTS_UNSUPPORTED);
                    ((ErrorPayload)response.Event.Payload).Message = "Thermostat does not support double setpoints";
                }
                else if (payload.TargetSetpoint != null && payload.LowerSetpoint != null && payload.UpperSetpoint != null)
                {
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.TRIPLE_SETPOINTS_UNSUPPORTED);
                    ((ErrorPayload)response.Event.Payload).Message = "Thermostat does not support triple setpoints";
                }
                else
                {
                    Setpoint setpoint = payload.TargetSetpoint;
                    // Convert setpoint to Celsius if Alexa gives setpoint in a different scale
                    setpoint = ConvertSetpoint(setpoint);

                    if (setpoint.Value < (double)Temperature.MINIMUM_VALUE || setpoint.Value > (double)Temperature.MAXIMUM_VALUE)
                    {
                        Setpoint minimumValue = new Setpoint((double)Temperature.MINIMUM_VALUE, Scale.CELSIUS);
                        Setpoint maximumValue = new Setpoint((double)Temperature.MAXIMUM_VALUE, Scale.CELSIUS);
                        TemperatureValidRange temperatureValidRange = new TemperatureValidRange(minimumValue, maximumValue);

                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.TEMPERATURE_VALUE_OUT_OF_RANGE);
                        ((TemperatureOutOfRangeErrorPayload)response.Event.Payload).Message = "Thermostat setpoint out of range";
                        ((TemperatureOutOfRangeErrorPayload)response.Event.Payload).ValidRange = temperatureValidRange;
                    }
                    else
                    {
                        // If last connection failed, use direct connection
                        ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, setpoint.Value, customer).ConfigureAwait(false);

                        if (connection == ConnectionState.OK)
                        {
                            response = new SmartHomeResponse(request.Directive.Header);
                            response.Event.Endpoint = new Endpoint()
                            {
                                EndpointID = device.Identifier
                            };
                            Context context = new Context();

                            Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                            context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                            context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));
                            context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 900000));

                            if (setpoint.Value <= device.TemperatureSetback
                                && device.TemperatureSetback != (double)Temperature.OFF
                                && device.TemperatureSetback != (double)Temperature.ON)
                            {
                                context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, ThermostatModes.ECO, DateTime.UtcNow, 900000));
                            }
                            else
                            {
                                context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, ThermostatModes.HEAT, DateTime.UtcNow, 900000));
                            }

                            PowerStates powerState;
                            if (device.TemperatureSetpoint == (double)Temperature.OFF)
                            {
                                powerState = PowerStates.OFF;
                            }
                            else
                            {
                                powerState = PowerStates.ON;
                            }

                            context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                            response.Context = context;
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Endpoint is unreachable
                            response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                            ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                        }
                        else
                        {
                            // Connection failed
                            response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                            ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                        }
                    }
                }
            }
            else if (request.Directive.Header.Name == HeaderNames.ADJUSTTARGETTEMPERATURE)
            {
                AdjustTargetTemperatureRequestPayload payload = (AdjustTargetTemperatureRequestPayload)request.Directive.Payload;

                // Convert setpoint to Celsius if Alexa gives setpoint in a different scale
                payload.TargetSetpointDelta = ConvertSetpoint(payload.TargetSetpointDelta);

                if (Math.Abs(payload.TargetSetpointDelta.Value) >= 0.5)
                {
                    if (device.TemperatureSetpoint != (double)Temperature.OFF)
                    {
                        double newTemperature = device.TemperatureSetpoint + payload.TargetSetpointDelta.Value;

                        if (newTemperature < (double)Temperature.MINIMUM_VALUE || newTemperature > (double)Temperature.MAXIMUM_VALUE)
                        {
                            Setpoint minimumValue = new Setpoint((double)Temperature.MINIMUM_VALUE, Scale.CELSIUS);
                            Setpoint maximumValue = new Setpoint((double)Temperature.MAXIMUM_VALUE, Scale.CELSIUS);
                            TemperatureValidRange temperatureValidRange = new TemperatureValidRange(minimumValue, maximumValue);

                            response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.TEMPERATURE_VALUE_OUT_OF_RANGE);
                            ((TemperatureOutOfRangeErrorPayload)response.Event.Payload).Message = "Thermostat setpoint out of range";
                            ((TemperatureOutOfRangeErrorPayload)response.Event.Payload).ValidRange = temperatureValidRange;
                        }
                        else
                        {
                            // If last connection failed, use direct connection
                            ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, newTemperature, customer).ConfigureAwait(false);

                            if (connection == ConnectionState.OK)
                            {
                                response = new SmartHomeResponse(request.Directive.Header);
                                response.Event.Endpoint = new Endpoint()
                                {
                                    EndpointID = device.Identifier
                                };
                                Context context = new Context();

                                Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                                context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                                Setpoint setpoint = new Setpoint(newTemperature, Scale.CELSIUS);
                                context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));
                                context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 900000));

                                if (setpoint.Value <= device.TemperatureSetback)
                                {
                                    context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, ThermostatModes.ECO, DateTime.UtcNow, 900000));
                                }
                                else
                                {
                                    context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, ThermostatModes.HEAT, DateTime.UtcNow, 900000));
                                }

                                PowerStates powerState;
                                if (device.TemperatureSetpoint == (double)Temperature.OFF)
                                {
                                    powerState = PowerStates.OFF;
                                }
                                else
                                {
                                    powerState = PowerStates.ON;
                                }

                                context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                                response.Context = context;
                            }
                            else if (connection == ConnectionState.FAILED)
                            {
                                // Endpoint is unreachable
                                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                                ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                            }
                            else
                            {
                                // Connection failed
                                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                                ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                            }
                        }
                    }
                    else
                    {
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.THERMOSTAT_IS_OFF);
                        ((ErrorPayload)response.Event.Payload).Message = "Cannot adjust thermostat because it is off";
                    }
                }
                else
                {
                    Setpoint setpoint = new Setpoint(0.5, Scale.CELSIUS);

                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.REQUESTED_SETPOINTS_TOO_CLOSE);
                    ((RequestedSetpointsTooCloseErrorPayload)response.Event.Payload).Message = "Requested temperature delta too low";
                    ((RequestedSetpointsTooCloseErrorPayload)response.Event.Payload).MinimumTemperatureDelta = setpoint;
                }
            }
            else if (request.Directive.Header.Name == HeaderNames.SET_THERMOSTATMODE)
            {
                SetThermostadModeRequestPayload payload = (SetThermostadModeRequestPayload)request.Directive.Payload;

                if (payload.ThermostatMode.Value == ThermostatModes.AUTO
                    || payload.ThermostatMode.Value == ThermostatModes.HEAT
                    || payload.ThermostatMode.Value == ThermostatModes.CUSTOM)
                {
                    // If last connection failed, use direct connection
                    ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, device.TemperatureComfort, customer).ConfigureAwait(false);

                    if (connection == ConnectionState.OK)
                    {
                        response = new SmartHomeResponse(request.Directive.Header);
                        response.Event.Endpoint = new Endpoint()
                        {
                            EndpointID = device.Identifier
                        };
                        Context context = new Context();

                        Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                        double temperatureSetpoint;
                        if (device.TemperatureComfort == (double)Temperature.ON)
                        {
                            temperatureSetpoint = 28;
                        }
                        else if (device.TemperatureComfort == (double)Temperature.OFF)
                        {
                            temperatureSetpoint = 8;
                        }
                        else
                        {
                            temperatureSetpoint = device.TemperatureComfort;
                        }

                        Setpoint setpoint = new Setpoint(temperatureSetpoint, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));

                        ThermostatModes thermostatMode;
                        if (device.TemperatureComfort == (double)Temperature.OFF)
                        {
                            thermostatMode = ThermostatModes.OFF;
                        }
                        else
                        {
                            thermostatMode = ThermostatModes.HEAT;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, thermostatMode, DateTime.UtcNow, 900000));
                        context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 900000));

                        PowerStates powerState;
                        if (device.TemperatureSetpoint == (double)Temperature.OFF)
                        {
                            powerState = PowerStates.OFF;
                        }
                        else
                        {
                            powerState = PowerStates.ON;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                        response.Context = context;
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Endpoint is unreachable
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                    else
                    {
                        // Connection failed
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                }
                else if (payload.ThermostatMode.Value == ThermostatModes.ECO
                    || payload.ThermostatMode.Value == ThermostatModes.COOL)
                {
                    ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, device.TemperatureSetback, customer).ConfigureAwait(false);

                    if (connection == ConnectionState.OK)
                    {
                        response = new SmartHomeResponse(request.Directive.Header);
                        response.Event.Endpoint = new Endpoint()
                        {
                            EndpointID = device.Identifier
                        };
                        Context context = new Context();

                        Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                        double temperatureSetpoint;
                        if (device.TemperatureSetback == (double)Temperature.ON)
                        {
                            temperatureSetpoint = 28;
                        }
                        else if (device.TemperatureSetback == (double)Temperature.OFF)
                        {
                            temperatureSetpoint = 8;
                        }
                        else
                        {
                            temperatureSetpoint = device.TemperatureSetback;
                        }

                        Setpoint setpoint = new Setpoint(temperatureSetpoint, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));

                        ThermostatModes thermostatMode;
                        if (device.TemperatureSetback == (double)Temperature.OFF)
                        {
                            thermostatMode = ThermostatModes.OFF;
                        }
                        else
                        {
                            thermostatMode = ThermostatModes.ECO;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, thermostatMode, DateTime.UtcNow, 900000));
                        context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 900000));

                        PowerStates powerState;
                        if (device.TemperatureSetpoint == (double)Temperature.OFF)
                        {
                            powerState = PowerStates.OFF;
                        }
                        else
                        {
                            powerState = PowerStates.ON;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                        response.Context = context;
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Endpoint is unreachable
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                    else
                    {
                        // Connection failed
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                }
                else if (payload.ThermostatMode.Value == ThermostatModes.OFF)
                {
                    // If last connection failed, use direct connection
                    ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, (double)Temperature.OFF, customer).ConfigureAwait(false);

                    if (connection == ConnectionState.OK)
                    {
                        response = new SmartHomeResponse(request.Directive.Header);
                        response.Event.Endpoint = new Endpoint()
                        {
                            EndpointID = device.Identifier
                        };
                        Context context = new Context();

                        Setpoint temperature = new Setpoint(device.Temperature, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_TEMPERATURESENSOR, PropertyNames.TEMPERATURE, temperature, DateTime.UtcNow, 900000));

                        Setpoint setpoint = new Setpoint((double)Temperature.MINIMUM_VALUE, Scale.CELSIUS);
                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.TARGET_SETPOINT, setpoint, DateTime.UtcNow, 900000));

                        context.Properties.Add(new Property(Namespaces.ALEXA_THERMOSTATCONTROLLER, PropertyNames.THERMOSTATMODE, ThermostatModes.OFF, DateTime.UtcNow, 900000));
                        context.Properties.Add(new Property(Namespaces.ALEXA_ENDPOINTHEALTH, PropertyNames.CONNECTIVITY, ConnectivityModes.OK, DateTime.UtcNow, 900000));

                        PowerStates powerState;
                        if (device.TemperatureSetpoint == (double)Temperature.OFF)
                        {
                            powerState = PowerStates.OFF;
                        }
                        else
                        {
                            powerState = PowerStates.ON;
                        }

                        context.Properties.Add(new Property(Namespaces.ALEXA_POWERCONTROLLER, PropertyNames.POWER_STATE, powerState, DateTime.UtcNow, 900000));

                        response.Context = context;
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Endpoint is unreachable
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                    else
                    {
                        // Connection failed
                        response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                        ((ErrorPayload)response.Event.Payload).Message = "Thermostat setpoint could not be set";
                    }
                }
                else
                {
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.UNSUPPORTED_THERMOSTAT_MODE);
                    ((ErrorPayload)response.Event.Payload).Message = "Thermostat does not know this mode";
                }
            }
            else
            {
                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.INVALID_DIRECTIVE);
                ((ErrorPayload)response.Event.Payload).Message = "Thermostat does not know this directive";
            }

            return response;
        }

        // Handle Alexa PowerController Directive
        // Set Smart Plug to the asked state if possible
        public async Task<SmartHomeResponse> HandleSceneControllerDirectiveAsync(SmartHomeRequest request, Device device, Customer customer)
        {
            SmartHomeResponse response;

            if (request.Directive.Header.Name == HeaderNames.ACTIVATE)
            {
                ConnectionState connectionState = await FritzBoxInstance.HandleApplyTemplateAsync(device, customer).ConfigureAwait(false);

                if (connectionState == ConnectionState.OK)
                {
                    response = new SmartHomeResponse(request.Directive.Header);
                    response.Event.Endpoint = new Endpoint()
                    {
                        EndpointID = device.Identifier
                    };
                    ((SceneStartedResponsePayload)response.Event.Payload).Cause = new Cause(CauseTypes.VOICE_INTERACTION);
                    ((SceneStartedResponsePayload)response.Event.Payload).Timestamp = DateTime.UtcNow;
                }
                else if (connectionState == ConnectionState.FAILED)
                {
                    // Endpoint is unreachable
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.ENDPOINT_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Scene could not be switched on";
                }
                else
                {
                    // Connection failed
                    response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.BRIDGE_UNREACHABLE);
                    ((ErrorPayload)response.Event.Payload).Message = "Scene could not be switched on";
                }
            }
            else
            {
                response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, ErrorTypes.INVALID_DIRECTIVE);
                ((ErrorPayload)response.Event.Payload).Message = "Scene does not know this directive";
            }

            return response;
        }

        // Create Error Event
        public SmartHomeResponse CreateErrorEvent(SmartHomeRequest request, ErrorTypes error, string errorMessage)
        {
            SmartHomeResponse response = SmartHomeResponse.CreateErrorResponse(request.Directive.Header, error);
            ((ErrorPayload)response.Event.Payload).Message = errorMessage;

            return response;
        }

        // Convert scale if necessary
        private Setpoint ConvertSetpoint(Setpoint setpoint)
        {
            // Convert from Fahrenheit to Celsius
            if (setpoint.Scale == Scale.FAHRENHEIT)
            {
                setpoint.Value = 5.0 / 9.0 * (setpoint.Value - 32.0);
                setpoint.Scale = Scale.CELSIUS;
            }
            // Convert from Kelvin to Celsius
            else if (setpoint.Scale == Scale.KELVIN)
            {
                setpoint.Value -= 273.15;
                setpoint.Scale = Scale.CELSIUS;
            }

            return setpoint;
        }
    }
}