﻿namespace FBSmartHome
{
    using Amazon;
    using Amazon.Runtime;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public sealed class OAuth
    {
        public static readonly OAuth Instance = new OAuth();

        private readonly HttpClient Client;

        static OAuth()
        {
        }

        private OAuth()
        {
            Client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(10)
            };
        }

        // Exchange code to customer access and refresh tokens
        public async Task<bool> ExchangeCodeForTokenAsync(string grantee, string code)
        {
            Logger.Instance.Debug("Exchanging token for grantee: {0}", grantee);

            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "grant_type", "authorization_code" },
                { "code", code },
                { "client_id", Environment.GetEnvironmentVariable("CLIENT_ID")},
                { "client_secret", Environment.GetEnvironmentVariable("CLIENT_SECRET")}
            };

            FormUrlEncodedContent content = new FormUrlEncodedContent(values);
            Logger.Instance.Debug("Exchanging Code.");

            HttpResponseMessage response;
            try
            {
                response = await Client.PostAsync("https://api.amazon.com/auth/o2/token", content).ConfigureAwait(false);
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not exchange token. Cannot proceed.");
                return false;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return false;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return false;
            }

            content?.Dispose();
            string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            bool responseOK = response.IsSuccessStatusCode;
            response?.Dispose();

            Logger.Instance.Debug("Code: {0}, Response: {1}", response.StatusCode, responseString);

            if (responseOK)
            {
                var definition = new
                {
                    access_token = string.Empty,
                    token_type = string.Empty,
                    expires_in = string.Empty,
                    refresh_token = string.Empty
                };

                var authResponse = JsonConvert.DeserializeAnonymousType(responseString, definition);

                // Get user ID
                string customerId = await GetCustomerIdAsync(grantee).ConfigureAwait(false);

                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                if (customer == null)
                {
                    // Database access failed, do not grant access
                    return false;
                }

                if (customer.CustomerId.Length <= 1)
                {
                    customer.CustomerId = customerId;
                }

                customer.AccessToken = authResponse.access_token;
                customer.RefreshToken = authResponse.refresh_token;
                double refreshTime = double.TryParse(authResponse.expires_in, out double refresh) ? refresh : 900.0;
                customer.TokenValidity = DateTime.UtcNow.AddSeconds(refreshTime);
                customer.UserRegion = Environment.GetEnvironmentVariable("REGION");

                PostgreSQL.Instance.SaveCustomer(customer);

                // Save token in cache
                Cache.Instance.StoreToken(grantee, customer.CustomerId);

                return true;
            }

            return false;
        }

        public async Task<string> GetCustomerIdAsync(string token)
        {
            string customerId = string.Empty;

            // Get customer id from LWA
            HttpResponseMessage response;
            try
            {
                response = await Client.GetAsync("https://api.amazon.com/user/profile?access_token=" + token.Trim()).ConfigureAwait(false);
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load oauth user id. Cannot proceed.");
                return string.Empty;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return string.Empty;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return string.Empty;
            }

            string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            bool responseOK = response.IsSuccessStatusCode;
            response?.Dispose();

            if (responseOK)
            {
                Logger.Instance.Debug("Response OK: {0}", responseString);

                var definition = new
                {
                    user_id = string.Empty
                };

                var idResponse = JsonConvert.DeserializeAnonymousType(responseString, definition);

                customerId = idResponse.user_id;
            }

            Logger.Instance.Log("Getting user id: {0}", customerId);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|oauth.getuser|", DateTimeOffset.Now.ToUnixTimeSeconds(), 1);

            return customerId;
        }

        // Exchange access token using the refresh token
        public async Task<bool> RefreshAccessTokenAsync(Customer customer)
        {
            if (customer.RefreshToken.Length <= 1)
            {
                return false;
            }

            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "grant_type", "refresh_token" },
                { "refresh_token", customer.RefreshToken },
                { "client_id", Environment.GetEnvironmentVariable("CLIENT_ID") },
                { "client_secret", Environment.GetEnvironmentVariable("CLIENT_SECRET") }
            };

            FormUrlEncodedContent content = new FormUrlEncodedContent(values);

            Logger.Instance.Debug("Refreshing token. Send content: {0}", await content.ReadAsStringAsync().ConfigureAwait(false));

            HttpResponseMessage response;
            try
            {
                response = await Client.PostAsync("https://api.amazon.com/auth/o2/token", content).ConfigureAwait(false);
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load token. Cannot proceed.");
                return false;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return false;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return false;
            }

            content?.Dispose();
            string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            bool responseOK = response.IsSuccessStatusCode;
            response?.Dispose();

            Logger.Instance.Debug("Code: {0}, Response: {1}", response.StatusCode, responseString);

            if (responseOK)
            {
                var definition = new
                {
                    access_token = string.Empty,
                    token_type = string.Empty,
                    expires_in = string.Empty,
                    refresh_token = string.Empty
                };

                var authResponse = JsonConvert.DeserializeAnonymousType(responseString, definition);

                customer.AccessToken = authResponse.access_token;
                customer.RefreshToken = authResponse.refresh_token;
                double refreshTime = double.TryParse(authResponse.expires_in, out double refresh) ? refresh : 900.0;
                customer.TokenValidity = DateTime.UtcNow.AddSeconds(refreshTime);

                PostgreSQL.Instance.SaveCustomer(customer);

                return true;
            }

            return false;
        }
    }
}