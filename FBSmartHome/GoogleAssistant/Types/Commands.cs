﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Commands
    {
        /// <summary>
        /// Float (in speech we generally take 1 decimal point, for Celsius users).
        /// </summary>
        public const string THERMOSTAT_TEMPERATURE_SETPOINT = "action.devices.commands.ThermostatTemperatureSetpoint";

        /// <summary>
        /// Floats. The high and low set points for a range. This works if and only if 
        /// the device supports heatcool mode, and we will set that mode if a range is requested.
        /// </summary>
        public const string THERMOSTAT_TEMPERATURE_SETRANGE = "action.devices.commands.ThermostatTemperatureSetRange";

        /// <summary>
        /// One of the supported modes on the device.
        /// </summary>
        public const string THERMOSTAT_SETMODE = "action.devices.commands.ThermostatSetMode";

        /// <summary>
        /// Whether to turn the device on or off.
        /// </summary>
        public const string ONOFF = "action.devices.commands.OnOff";

        /// <summary>
        /// Whether to activate or deactivate the scene
        /// </summary>
        public const string ACTIVATE_SCENE = "action.devices.commands.ActivateScene";
    }
}
