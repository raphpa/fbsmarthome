﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class IntentNames
    {
        /// <summary>
        /// Intent Name for the SYNC Intent
        /// </summary>
        public const string SYNC = "action.devices.SYNC";

        /// <summary>
        /// Intent Name for the QUERY Intent
        /// </summary>
        public const string QUERY = "action.devices.QUERY";

        /// <summary>
        /// Intent Name for the EXECUTE Intent
        /// </summary>
        public const string EXECUTE = "action.devices.EXECUTE";

        /// <summary>
        /// Intent Name for the DISCONNECT Intent
        /// </summary>
        public const string DISCONNECT = "action.devices.DISCONNECT";
    }
}
