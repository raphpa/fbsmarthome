﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class States
    {
        /// <summary>
        /// Device Type CAMERA
        /// </summary>
        public const string ON = "on";

        /// <summary>
        /// Current mode of the device, with the same values as the attribute
        /// </summary>
        public const string THERMOSTAT_MODE = "thermostatMode";

        /// <summary>
        /// Current temperature set point (single target), in C
        /// </summary>
        public const string THERMOSTAT_SETPOINT = "thermostatTemperatureSetpoint";

        /// <summary>
        /// Current observe temperature, in C
        /// </summary>
        public const string THERMOSTAT_TEMPERATURE = "thermostatTemperatureAmbient";

        /// <summary>
        /// Current high point if in heatcool mode, for a range
        /// </summary>
        public const string THERMOSTAT_SETPOINT_HIGH = "thermostatTemperatureSetpointHigh";

        /// <summary>
        /// Current low point if in heatcool mode, for a range
        /// </summary>
        public const string THERMOSTAT_SETPOINT_LOW = "thermostatTemperatureSetpointLow";

        /// <summary>
        /// Float. 0-100. If available from the device.
        /// </summary>
        public const string THERMOSTAT_HUMIDITY = "thermostatHumidityAmbient";

        /// <summary>
        /// Bool. Device is online
        /// </summary>
        public const string ONLINE = "online";

        /// <summary>
        /// You should return an exception when there is an issue or alert associated with a command. The command could succeed or fail.
        /// </summary>
        public const string EXCEPTIONCODE = "exceptionCode";
    }
}
