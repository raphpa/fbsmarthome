﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class ErrorCodes
    {
        /// <summary>
        /// Error Code authExpired: Credentials have expired.
        /// </summary>
        public const string AUTHEXPIRED = "authExpired";

        /// <summary>
        /// Error Code authFailure: General failure to authenticate.
        /// </summary>
        public const string AUTHFAILURE = "authFailure";

        /// <summary>
        /// Error Code deviceOffline: The target is unreachable.
        /// </summary>
        public const string DEVICEOFFLINE = "deviceOffline";

        /// <summary>
        /// Error Code timeout: Internal timeout.
        /// </summary>
        public const string TIMEOUT = "timeout";

        /// <summary>
        /// Error Code deviceTurnedOff: The device is known to be turned hard off (if distinguishable from unreachable).
        /// </summary>
        public const string DEVICETURNEDOFF = "deviceTurnedOff";

        /// <summary>
        /// Error Code deviceNotFound: The device doesn't exist on the partner's side. 
        /// This normally indicates a failure in data synchronization or a race condition.
        /// </summary>
        public const string DEVICENOTFOUND = "deviceNotFound";

        /// <summary>
        /// Error Code valueOutOfRange: The range in parameters is out of bounds.
        /// </summary>
        public const string VALUEOUTOFRANGE = "valueOutOfRange";

        /// <summary>
        /// Error Code notSupported: The command or its parameters are unsupported (this should generally not happen, 
        /// as traits and business logic should prevent it).
        /// </summary>
        public const string NOTSUPPORTED = "notSupported";

        /// <summary>
        /// Error Code protocolError: Failure in processing the request.
        /// </summary>
        public const string PROTOCOLERROR = "protocolError";

        /// <summary>
        /// Error Code unknownError: Everything else, although anything that throws this should be replaced with a real error code.
        /// </summary>
        public const string UNKNOWNERROR = "unknownError";

        /// <summary>
        /// <device(s)> <has/have> low battery.
        /// </summary>
        public const string LOWBATTERY = "lowBattery";

        /// <summary>
        /// <device(s)> <doesn't/don't> support remote control.
        /// </summary>
        public const string DIRECTRESPONSEONLYUNREACHABLE = "directResponseOnlyUnreachable";
    }
}
