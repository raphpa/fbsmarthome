﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class TemperatureUnit
    {
        /// <summary>
        /// Celsius
        /// </summary>
        public const string C = "C";

        /// <summary>
        /// Fahrenheit
        /// </summary>
        public const string F = "F";
    }
}
