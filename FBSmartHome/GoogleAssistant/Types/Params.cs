﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Params
    {
        /// <summary>
        /// Float (in speech we generally take 1 decimal point, for Celsius users).
        /// </summary>
        public const string TEMPERATURE_SETPOINT = "thermostatTemperatureSetpoint";

        /// <summary>
        /// Floats. The high and low set points for a range. This works if and only if 
        /// the device supports heatcool mode, and we will set that mode if a range is requested.
        /// </summary>
        public const string TEMPERATURE_SETPOINT_HIGH = "thermostatTemperatureSetpointHigh";

        /// <summary>
        /// Floats. The high and low set points for a range. This works if and only if 
        /// the device supports heatcool mode, and we will set that mode if a range is requested.
        /// </summary>
        public const string TEMPERATURE_SETPOINT_LOW = "thermostatTemperatureSetpointLow";

        /// <summary>
        /// One of the supported modes on the device.
        /// </summary>
        public const string THERMOSTAT_MODE = "thermostatMode";

        /// <summary>
        /// Boolean. Required. Whether to turn the device on or off.
        /// </summary>
        public const string ON = "on";

        /// <summary>
        /// Boolean. True if scene is reversible and should be reversed.
        /// </summary>
        public const string DEACTIVATE = "deactivate";
    }
}
