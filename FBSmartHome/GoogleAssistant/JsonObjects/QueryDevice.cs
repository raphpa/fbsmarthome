﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class QueryDevice
    {
        /// <summary>
        /// Required. Partner ID to query, as per the id provided in SYNC.
        /// </summary>
        [JsonProperty("id")]
        [JsonRequired]
        public string Id { get; set; }

        /// <summary>
        /// Optional. If the opaque customData object is provided in SYNC, it's sent here.
        /// </summary>
        [JsonProperty("customData")]
        public Dictionary<string, object> CustomData { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public QueryDevice()
        {
        }
    }
}
