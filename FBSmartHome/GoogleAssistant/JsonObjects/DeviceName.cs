﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class DeviceName
    {
        /// <summary>
        /// Array<String>. Optional. List of names provided by the partner rather 
        /// than the user, often manufacturer names, SKUs, etc.
        /// </summary>
        [JsonProperty("defaultNames", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> DefaultNames { get; set; }

        /// <summary>
        /// String. Optional. Primary name of the device, generally provided by the user. 
        /// This is also the name the Assistant will prefer to describe the device in responses.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Array<String>. Optional. Additional names provided by the user for the device.
        /// </summary>
        [JsonProperty("nickNames", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> NickNames { get; set; }
    }
}
