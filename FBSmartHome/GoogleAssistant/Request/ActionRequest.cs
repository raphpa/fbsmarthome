﻿namespace FBSmartHome.GoogleAssistant.Request
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    /// <summary>
    /// Request from Google Assistant
    /// </summary>
    public class ActionRequest
    {
        /// <summary>
        /// RequestId
        /// </summary>
        [JsonProperty("requestId")]
        [JsonRequired]
        public string RequestId { get; set; }

        /// <summary>
        /// Inputs
        /// </summary>
        [JsonProperty("inputs")]
        [JsonRequired]
        public List<Input> Inputs { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public ActionRequest()
        {
        }
    }
}
