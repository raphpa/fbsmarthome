﻿namespace FBSmartHome.GoogleAssistant.Request
{
    using FBSmartHome.GoogleAssistant.Payloads;
    using Newtonsoft.Json;

    /// <summary>
    /// Request from Google Assistant
    /// </summary>
    [JsonConverter(typeof(InputConverter))]
    public class Input
    {
        /// <summary>
        /// RequestId
        /// </summary>
        [JsonProperty("intent")]
        [JsonRequired]
        public string Intent { get; set; } = string.Empty;

        /// <summary>
        /// Payload
        /// </summary>
        [JsonProperty("payload")]
        public Payload Payload { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public Input()
        {
        }
    }
}
