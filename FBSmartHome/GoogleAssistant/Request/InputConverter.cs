﻿namespace FBSmartHome.GoogleAssistant.Request
{
    using FBSmartHome.GoogleAssistant.Payloads;
    using FBSmartHome.GoogleAssistant.Payloads.Response;
    using FBSmartHome.GoogleAssistant.Types;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;

    /// <summary>
    /// JsonConverter for the creation of Google Assistant Inputs
    /// </summary>
    public class InputConverter : JsonConverter
    {
        /// <summary>
        /// Converts Google Assistant input
        /// </summary>
        public override bool CanWrite => false;

        /// <summary>
        /// Returns, if the object can be converted
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(System.Type objectType) => objectType == typeof(Input);

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        /// <exception cref="NotImplementedException"></exception>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
#pragma warning disable RCS1079 // Throwing of new NotImplementedException.
            throw new NotImplementedException();
#pragma warning restore RCS1079 // Throwing of new NotImplementedException.
        }

        /// <summary>
        /// Reads Json in a Objekt und creates a SmartHomeRequest
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            string intent = jObject["intent"]?.Value<string>();
            object req = null;
            if (!string.IsNullOrEmpty(intent))
            {
                req = new Input()
                {
                    Payload = CreatePayload(intent)
                };
                // (req as Input).Payload = CreatePayload(intent);
            }
            else
            {
                throw new InvalidOperationException("(InputConverter)Empty Intent. Object: " + jObject);
            }

            serializer.Populate(jObject.CreateReader(), req);

            return req;
        }

        private Payload CreatePayload(string intent)
        {
            switch (intent)
            {
                case IntentNames.SYNC:
                    return new Payload();
                case IntentNames.QUERY:
                    return new QueryPayload();
                case IntentNames.EXECUTE:
                    return new ExecutePayload();
                case IntentNames.DISCONNECT:
                    return new DisconnectResponsePayload();
                default:
                    throw new ArgumentOutOfRangeException(nameof(Type), $"Unknown intent: {intent}.");
            }
        }
    }
}