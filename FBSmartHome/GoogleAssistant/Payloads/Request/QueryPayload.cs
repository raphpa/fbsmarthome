﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using FBSmartHome.GoogleAssistant.JsonObjects;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    /// <summary>
    /// Query payload for request
    /// </summary>
    public class QueryPayload : Payload
    {
        /// <summary>
        /// Devices state is queried for
        /// </summary>
        [JsonProperty("devices")]
        [JsonRequired]
        public List<QueryDevice> Devices { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public QueryPayload()
        {
            Devices = new List<QueryDevice>();
        }
    }
}
