﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    /// <summary>
    /// Query payload for response
    /// </summary>
    public class QueryResponsePayload : Payload
    {
        /// <summary>
        /// String. Optional. An error code for the entire transaction -- for auth failures and partner 
        /// system unavailability. For individual device errors use the errorCode within the device object.
        /// </summary>
        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorCode { get; set; }

        /// <summary>
        /// String. Optional. Detailed error which will never be presented to users but may be logged or used during development.
        /// </summary>
        [JsonProperty("debugString", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugString { get; set; }

        /// <summary>
        /// Object. Map of devices.
        /// </summary>
        [JsonProperty("devices")]
        [JsonRequired]
        public Dictionary<string, object> Devices { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public QueryResponsePayload()
        {
            Devices = new Dictionary<string, object>();
        }
    }
}
