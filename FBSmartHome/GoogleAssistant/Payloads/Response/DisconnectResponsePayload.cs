﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using Newtonsoft.Json;

    /// <summary>
    /// Disconnect payload for response
    /// </summary>
    public class DisconnectResponsePayload : Payload
    {
        /// <summary>
        /// String. Optional. For systematic errors on DISCONNECT.
        /// </summary>
        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorCode { get; set; }

        /// <summary>
        /// String. Optional. Detailed error which will never be presented to users but may be logged or 
        /// used during development.
        /// </summary>
        [JsonProperty("debugString", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugString { get; set; }

        /// <summary>
        /// String (up to 256 bytes). Optional. Reﬂects the unique (and immutable) user ID on the agent's platform. 
        /// The string is opaque to Google, so if there's an immutable form vs a mutable form on the agent side, 
        /// use the immutable form (e.g. an account number rather than email).
        /// </summary>
        [JsonProperty("agentUserId", NullValueHandling = NullValueHandling.Ignore)]
        public string AgentUserId { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public DisconnectResponsePayload()
        {
        }
    }
}
