﻿namespace FBSmartHome
{
    using Npgsql;
    using NpgsqlTypes;
    using System;
    using System.Collections.Generic;

    public sealed class PostgreSQL
    {
        public static readonly PostgreSQL Instance = new PostgreSQL();

        private readonly NpgsqlConnection pqsqlConnection;

        static PostgreSQL()
        {
        }

        private PostgreSQL()
        {
            try
            {
                NpgsqlConnection.GlobalTypeMapper.UseJsonNet();
                pqsqlConnection = new NpgsqlConnection(Environment.GetEnvironmentVariable("PGSQL_CONNECTION"));
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception while creating sql connection: {0}", ex);
            }
        }

        // Get customer from DB using customer id as hash key
        public Customer GetCustomer(string customerId)
        {
            try
            {
                // Open connection
                if (pqsqlConnection.State != System.Data.ConnectionState.Open)
                {
                    pqsqlConnection.Open();
                }

                // Query database for customer
                using (NpgsqlCommand command = new NpgsqlCommand("SELECT * FROM fbsmarthome WHERE customerid = @id", pqsqlConnection))
                {
                    command.Parameters.AddWithValue("id", customerId);
                    NpgsqlDataReader reader = command.ExecuteReader();               

                    Customer customer = new Customer();

                    // If customer was found
                    if (reader.HasRows)
                    {
                        // Get next result
                        reader.Read();

                        // set data
                        customer.CustomerId = customerId;
                        customer.AccessToken = reader.IsDBNull(reader.GetOrdinal("accesstoken")) ? string.Empty : reader.GetString(reader.GetOrdinal("accesstoken"));
                        customer.RefreshToken = reader.IsDBNull(reader.GetOrdinal("refreshtoken")) ? string.Empty : reader.GetString(reader.GetOrdinal("refreshtoken"));
                        customer.TokenValidity = reader.IsDBNull(reader.GetOrdinal("tokenvalidity")) ? DateTime.UnixEpoch : DateTime.Parse(reader.GetString(reader.GetOrdinal("tokenvalidity")));
                        customer.ApiKey = reader.IsDBNull(reader.GetOrdinal("apikey")) ? string.Empty : reader.GetString(reader.GetOrdinal("apikey"));
                        customer.IsGoogleAssistant = reader.IsDBNull(reader.GetOrdinal("isgoogleassistant")) ? false : (reader.GetInt32(reader.GetOrdinal("isgoogleassistant")) == 1);
                        customer.UserRegion = reader.IsDBNull(reader.GetOrdinal("userregion")) ? string.Empty : reader.GetString(reader.GetOrdinal("userregion"));
                        customer.DeviceList = reader.IsDBNull(reader.GetOrdinal("devicelist")) ? new List<Device>() : reader.GetFieldValue<List<Device>>(reader.GetOrdinal("devicelist"));
                        customer.ConnectionList = reader.IsDBNull(reader.GetOrdinal("connectionlist")) ? new List<Connection>() : reader.GetFieldValue<List<Connection>>(reader.GetOrdinal("connectionlist"));
                        customer.TTL = reader.IsDBNull(reader.GetOrdinal("ttl")) ? 0 : reader.GetInt64(reader.GetOrdinal("ttl"));
                    }
                    else
                    {
                        Logger.Instance.Debug("Customer not found, creating new customer");
                    }

                    // dispose reader
                    reader.Close();

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|db.read|#id", DateTimeOffset.Now.ToUnixTimeSeconds(), 1);

                    return customer;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception while performing sql command: {0}", ex);
                pqsqlConnection.Close();
            }

            return null;
        }

        // Save customer to db
        public bool SaveCustomer(Customer customer)
        {
            if(customer == null)
            {
                return false;
            }

            try
            {
                // Open connection
                if (pqsqlConnection.State != System.Data.ConnectionState.Open)
                {
                    pqsqlConnection.Open();
                }

                // Update TTL
                customer.TTL = ((DateTimeOffset)DateTime.UtcNow.AddMonths(3)).ToUnixTimeSeconds();

                // Upsert customer
                // Try insert first, if customer does exist, update
                using (NpgsqlCommand command = new NpgsqlCommand("INSERT INTO fbsmarthome (customerid, userregion, isgoogleassistant, " +
                                                                                    "connectionlist, devicelist, accesstoken, refreshtoken, tokenvalidity, apikey, ttl) " +
                                                                                    "VALUES (@customerid, @userregion, @isgoogleassistant, " +
                                                                                    "@connectionlist, @devicelist, @accesstoken, @refreshtoken, @tokenvalidity, @apikey, @ttl) " +
                                                                                    "ON CONFLICT (customerid) DO UPDATE SET " +
                                                                                    "userregion = @userregion, isgoogleassistant = @isgoogleassistant, " +
                                                                                    "connectionlist = @connectionlist, devicelist = @devicelist, accesstoken = @accesstoken, refreshtoken = @refreshtoken, " +
                                                                                    "tokenvalidity = @tokenvalidity, apikey = @apikey, ttl = @ttl",
                                                                                    pqsqlConnection))
                {
                    command.Parameters.Add(new NpgsqlParameter("customerid", NpgsqlDbType.Text) { Value = customer.CustomerId });
                    command.Parameters.Add(new NpgsqlParameter("userregion", NpgsqlDbType.Text) { Value = customer.UserRegion });
                    command.Parameters.Add(new NpgsqlParameter("isgoogleassistant", NpgsqlDbType.Numeric) { Value = customer.IsGoogleAssistant ? 1 : 0 });
                    command.Parameters.Add(new NpgsqlParameter("connectionlist", NpgsqlDbType.Jsonb) { Value = customer.ConnectionList });
                    command.Parameters.Add(new NpgsqlParameter("devicelist", NpgsqlDbType.Jsonb) { Value = customer.DeviceList });
                    command.Parameters.Add(new NpgsqlParameter("accesstoken", NpgsqlDbType.Text) { Value = customer.AccessToken });
                    command.Parameters.Add(new NpgsqlParameter("refreshtoken", NpgsqlDbType.Text) { Value = customer.RefreshToken });
                    command.Parameters.Add(new NpgsqlParameter("tokenvalidity", NpgsqlDbType.Text) { Value = customer.TokenValidity.ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'") });
                    command.Parameters.Add(new NpgsqlParameter("apikey", NpgsqlDbType.Text) { Value = customer.ApiKey });
                    command.Parameters.Add(new NpgsqlParameter("ttl", NpgsqlDbType.Numeric) { Value = customer.TTL });

                    command.ExecuteNonQuery();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|db.write|", DateTimeOffset.Now.ToUnixTimeSeconds(), 1);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception while performing sql command: {0}", ex);
                pqsqlConnection.Close();
            }

            return false;
        }
    }
}
