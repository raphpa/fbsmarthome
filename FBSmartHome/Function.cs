namespace FBSmartHome
{
    using Amazon.Lambda.APIGatewayEvents;
    using FBSmartHome.GoogleAssistant.JsonObjects;
    using FBSmartHome.GoogleAssistant.Payloads.Response;
    using FBSmartHome.GoogleAssistant.Request;
    using FBSmartHome.GoogleAssistant.Response;
    using FBSmartHome.GoogleAssistant.Types;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using RKon.Alexa.NET46.Payloads;
    using RKon.Alexa.NET46.Request;
    using RKon.Alexa.NET46.Response;
    using RKon.Alexa.NET46.Types;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;

    // Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
    // [assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

    public class Function
    {
        private IFritzBox FritzBoxInstance;

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        public Stream FunctionHandler(Stream input)
        {
            DateTime startTime = DateTime.UtcNow;

            if (Environment.GetEnvironmentVariable("USE_SNS") == "true")
            {
                FritzBoxInstance = FritzBoxViaRabbitMQ.Instance;
            }
            else
            {
                FritzBoxInstance = FritzBox.Instance;
            }

            // Handle incoming request
            string inputString;
            using (StreamReader streamReader = new StreamReader(input))
            {
                inputString = streamReader.ReadToEnd();
            }
            string responseString = string.Empty;

            // Read input
            SmartHomeRequest directive = JsonConvert.DeserializeObject<SmartHomeRequest>(inputString);
            if (directive.Directive?.Header?.Namespace?.StartsWith(Namespaces.ALEXA) == true)
            {
                // Input is Alexa request
                Logger.Instance.Log("Received directive: {0}", JsonConvert.SerializeObject(directive, new StringEnumConverter()));

                // Handle Directive
                SmartHomeResponse response = DirectiveHandlerAsync(directive).GetAwaiter().GetResult();

                // Return response
                responseString = JsonConvert.SerializeObject(response, new StringEnumConverter());
                Logger.Instance.Log("Sending response: {0}", responseString);
            }
            else
            {
                // Input is request from API Gateway
                APIGatewayProxyRequest proxyRequest = JsonConvert.DeserializeObject<APIGatewayProxyRequest>(inputString);
                Logger.Instance.Debug("Received proxy request: {0}", JsonConvert.SerializeObject(proxyRequest, new StringEnumConverter()));

                string token = proxyRequest.Headers.TryGetValue("Authorization", out token) ? token.Replace("Bearer ", "") : string.Empty;
                string apiVersion = proxyRequest.Headers.TryGetValue("Google-Assistant-API-Version", out apiVersion) ? apiVersion : string.Empty;
                string body = proxyRequest.Body;

                // Check for correct API version
                if (apiVersion == "v1" && body != string.Empty)
                {
                    // Input is Google Assistant request
                    ActionRequest actionRequest = JsonConvert.DeserializeObject<ActionRequest>(body);
                    Logger.Instance.Log("Received action request: {0}", JsonConvert.SerializeObject(actionRequest, new StringEnumConverter()));

                    // Handle Action
                    ActionResponse response = ActionHandlerAsync(actionRequest, token).GetAwaiter().GetResult();

                    // Return response
                    // Serialize Action Response into string
                    responseString = (response == null) ? string.Empty : JsonConvert.SerializeObject(response, new StringEnumConverter());

                    Logger.Instance.Debug("Sending response: {0}", responseString);

                    // Create API Gateway response with action response in body
                    APIGatewayProxyResponse proxyResponse = new APIGatewayProxyResponse
                    {
                        IsBase64Encoded = false,
                        StatusCode = 200,
                        Headers = new Dictionary<string, string>(),
                        Body = responseString
                    };

                    // Serialize API Gateway response
                    responseString = JsonConvert.SerializeObject(proxyResponse, new StringEnumConverter());
                    Logger.Instance.Log("Sending proxy response: {0}", responseString);
                }
                else
                {
                    // Invalid call from API Gateway, send error
                }
            }

            // Add custom metric for datadog logging
            double timeElapsed = (DateTime.UtcNow - startTime).TotalMilliseconds;
            double timeBilled = Math.Ceiling(timeElapsed / 100.0) * 100;
            Logger.Instance.Log("MONITORING|{0}|{1}|count|lambda.time.elapsed", DateTimeOffset.Now.ToUnixTimeSeconds(), timeElapsed);
            Logger.Instance.Log("MONITORING|{0}|{1}|count|lambda.time.billed", DateTimeOffset.Now.ToUnixTimeSeconds(), timeBilled);

            return new MemoryStream(Encoding.UTF8.GetBytes(responseString));
        }

        public async Task<SmartHomeResponse> DirectiveHandlerAsync(SmartHomeRequest request)
        {
            // Check if directive is Grant Directive. If so, send to Grant Handler
            if (request.Directive.Header.Name == HeaderNames.ACCEPT_GRANT)
            {
                // Grand handler will get access token and add user to database
                SmartHomeResponse response = await Alexa.Instance.HandleAcceptGrantDirectiveAsync(request).ConfigureAwait(false);
                return response;
            }

            // Check if directive is Discovery Directive
            if (request.Directive.Header.Name == HeaderNames.DISCOVERY_REQUEST)
            {
                // Get token from payload scope
                string token = ((RequestPayloadWithScope)request.Directive.Payload).Scope.Token;
                // If token is empty, return empty list. Discovery never returns error
                {
                    if (string.IsNullOrEmpty(token))
                    {
                        Logger.Instance.Debug("Customer empty");
                        List<Device> list = new List<Device>();
                        return Alexa.Instance.HandleDiscoveryDirective(request, new Customer());
                    }
                }

                // Try to find token in token cache
                Logger.Instance.Debug("Search customer: {0}", token);

                string customerId = Cache.Instance.CheckToken(token);

                if (customerId != null && customerId?.Length > 1)
                {
                    Logger.Instance.Debug("Found customer in cache.");
                }
                else
                {
                    // Query oauth for user
                    Logger.Instance.Debug("Customer not found in cache, querying oauth");
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                    if (customerId.Length <= 1)
                    {
                        // Getting customer id from oauth token failed
                        Logger.Instance.Debug("Could not get customer id");
                        return Alexa.Instance.HandleDiscoveryDirective(request, new Customer());
                    }

                    // Save token in cache
                    Cache.Instance.StoreToken(token, customerId);
                }

                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    // Customer not in database
                    return Alexa.Instance.HandleDiscoveryDirective(request, new Customer());
                }

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                // If user could not be found, return empty list. Discovery never returns error
                if (!isValidUri)
                {
                    Logger.Instance.Debug("Customer not found or no valid uri");
                    List<Device> list = new List<Device>();
                    return Alexa.Instance.HandleDiscoveryDirective(request, customer);
                }

                // Try to query for devices if list is old
                ConnectionState connection;
                if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(60) < DateTime.UtcNow
                    || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                {
                    connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), false).ConfigureAwait(false);
                    customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices = DateTime.UtcNow;
                    PostgreSQL.Instance.SaveCustomer(customer);
                }

                // Get response for Alexa
                SmartHomeResponse response = Alexa.Instance.HandleDiscoveryDirective(request, customer);

                return response;
            }

            // Check if directive is Report State Directive
            if (request.Directive.Header.Name == HeaderNames.REPORT_STATE)
            {
                // Get token and identifier from endpoint
                string token = request.Directive.Endpoint.Scope.Token;
                string identifier = request.Directive.Endpoint.EndpointID;

                string customerId;
                // Check if cookie with customer ID was transmitted
                if (request.Directive.Endpoint.Cookie.ContainsKey("customerId"))
                {
                    customerId = request.Directive.Endpoint.Cookie["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.INVALID_AUTHORIZATION_CREDENTIAL, "There was no token transmitted");
                }

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Connection failed. Invalid URI.");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                }

                // Try to find device
                Logger.Instance.Debug("Search endpoint: {0}", identifier);
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);
                ConnectionNumber connectionNumber;
                if (deviceItem != null)
                {
                    connectionNumber = deviceItem.ConnectionNumber;
                }
                else
                {
                    connectionNumber = ConnectionNumber.DEFAULT;
                }

                // Get device if it is not found
                if (deviceItem == null)
                {
                    if (customer.GetConnection(connectionNumber).LastUpdatedDevices.AddMinutes(2) < DateTime.UtcNow)
                    {
                        await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(connectionNumber), false).ConfigureAwait(false);
                        customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices = DateTime.UtcNow;
                        PostgreSQL.Instance.SaveCustomer(customer);
                    }

                    Logger.Instance.Debug("Endpoint not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                }
  
                // Try to query for devices
                // Get cached device list if cache is not outdated (<10min)
                // Else send deferred message and query fritz!box before answering
                // Also query if last try was not successfull
                if (customer.GetConnection(connectionNumber).LastUpdatedDevices.AddMinutes(10) < DateTime.UtcNow
                        || (customer.GetConnection(connectionNumber).LastConnectionState != ConnectionState.OK &&
                            customer.GetConnection(connectionNumber).LastUpdatedDevices.AddMinutes(2) < DateTime.UtcNow)
                   )
                {
                    ConnectionState connection;

                    Logger.Instance.Debug("Device outdated, update via deferred state report");
                    connection =  await FritzBoxViaRabbitMQ.Instance.HandleStateReportAsync(request.Directive.Header.CorrelationToken, customer, deviceItem).ConfigureAwait(false);

                    customer.GetConnection(connectionNumber).LastUpdatedDevices = DateTime.UtcNow;
                    PostgreSQL.Instance.SaveCustomer(customer);

                    if (connection != ConnectionState.OK)
                    {
                        Logger.Instance.Debug("Connection failed.");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                    }

                    return Alexa.Instance.HandleDeferredReportStateDirective(request);
                }

                if (customer.GetConnection(connectionNumber).LastConnectionState != ConnectionState.OK)
                {
                    Logger.Instance.Debug("Connection failed.");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                }

                Logger.Instance.Debug("Device found");
                // Handle state report
                return Alexa.Instance.HandleReportStateDirective(request, customer, deviceItem);
            }

            // Check if directive is Power Controller Directive
            if (request.Directive.Header.Namespace == Namespaces.ALEXA_POWERCONTROLLER)
            {
                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.access|#service:{2},request",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, "alexa");

                // Get token and identifier from endpoint
                string token = request.Directive.Endpoint.Scope.Token;
                string identifier = request.Directive.Endpoint.EndpointID;

                string customerId;
                // Check if cookie with customer ID was transmitted
                if (request.Directive.Endpoint.Cookie.ContainsKey("customerId"))
                {
                    customerId = request.Directive.Endpoint.Cookie["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer =  PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.INVALID_AUTHORIZATION_CREDENTIAL, "There was no token transmitted");
                }

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Connection failed. Invalid URI.");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                }

                // Get device
                Logger.Instance.Debug("Search endpoint: {0}", identifier);
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));

                // If device does not exist, send error
                if (deviceItem == null)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), true).ConfigureAwait(false);
                    }

                    Logger.Instance.Debug("Endpoint not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                }

                // If device is offline and on low battery, send error
                if (!deviceItem.Present && deviceItem.BatteryLow)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));
                    // If device still is not present, end with error
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (!deviceItem.Present)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.ENDPOINT_LOW_POWER, "Endpoint is not reachable.");
                    }
                }

                // If device is offline, send error
                if (!deviceItem.Present)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));
                    // If device sill is not present, end with error
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (!deviceItem.Present)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.ENDPOINT_UNREACHABLE, "Endpoint is not reachable.");
                    }
                }

                // If device is locked, send error
                if (deviceItem.Locked)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));
                    // If device sill is locked, end with error
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (deviceItem.Locked)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.CLOUD_CONTROL_DISABLED, "Endpoint is locked.");
                    }
                }

                // Handle Power Controller Directive
                SmartHomeResponse response = await Alexa.Instance.HandlePowerControllerDirectiveAsync(request, deviceItem, customer).ConfigureAwait(false);

                return response;
            }

            // Check if directive is Thermostat Controller Directive
            if (request.Directive.Header.Namespace == Namespaces.ALEXA_THERMOSTATCONTROLLER)
            {
                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.access|#service:{2},request",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, "alexa");

                // Get token and identifier from endpoint
                string token = request.Directive.Endpoint.Scope.Token;
                string identifier = request.Directive.Endpoint.EndpointID;

                string customerId;
                // Check if cookie with customer ID was transmitted
                if (request.Directive.Endpoint.Cookie.ContainsKey("customerId"))
                {
                    customerId = request.Directive.Endpoint.Cookie["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.INVALID_AUTHORIZATION_CREDENTIAL, "There was no token transmitted");
                }

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Connection failed. Invalid URI.");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                }

                // Get device
                Logger.Instance.Debug("Search endpoint: {0}", identifier);
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);

                // If device does not exist, send error
                if (deviceItem == null)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), true).ConfigureAwait(false);
                    }

                    // If device sill is does not exist, end with error
                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                }

                // If device is offline and on low battery, send error
                if (!deviceItem.Present && deviceItem.BatteryLow)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    // If device sill is not present, end with error
                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (!deviceItem.Present)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.ENDPOINT_LOW_POWER, "Endpoint is not reachable.");
                    }
                }

                // If device is offline, send error
                if (!deviceItem.Present)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    // If device sill is not present, end with error
                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (!deviceItem.Present)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.ENDPOINT_UNREACHABLE, "Endpoint is not reachable.");
                    }
                }

                // If device is locked, send error
                if (deviceItem.Locked)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
                    }

                    // If device sill is locked, end with error
                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier);
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                    else if (deviceItem.Locked)
                    {
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.CLOUD_CONTROL_DISABLED, "Endpoint is locked.");
                    }
                }

                // Handle Thermostet Controller Directive
                SmartHomeResponse response = await Alexa.Instance.HandleThermostatDirectiveAsync(request, deviceItem, customer).ConfigureAwait(false);

                return response;
            }

            // Check if directive is Scene Controller Directive
            if (request.Directive.Header.Namespace == Namespaces.ALEXA_SCENECONTROLLER)
            {
                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.access|#service:{2},request",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, "alexa");

                // Get token and identifier from endpoint
                string token = request.Directive.Endpoint.Scope.Token;
                string identifier = request.Directive.Endpoint.EndpointID;

                string customerId;
                // Check if cookie with customer ID was transmitted
                if (request.Directive.Endpoint.Cookie.ContainsKey("customerId"))
                {
                    customerId = request.Directive.Endpoint.Cookie["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.INVALID_AUTHORIZATION_CREDENTIAL, "There was no token transmitted");
                }

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Connection failed. Invalid URI.");
                    return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.BRIDGE_UNREACHABLE, "Bridge could not be reached.");
                }

                // Get device
                Logger.Instance.Debug("Search endpoint: {0}", identifier);
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));

                // If device does not exist, send error
                if (deviceItem == null)
                {
                    // Try to update device list if it is old
                    if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                        || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                    {
                        ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), true).ConfigureAwait(false);
                    }

                    deviceItem = customer.DeviceList.Find(device => device.Identifier == identifier.Replace("-virtual", ""));
                    // If device sill does not exist, exit with error
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Endpoint not found");
                        return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.NO_SUCH_ENDPOINT, "Endpoint could not be found.");
                    }
                }

                // Handle Scene Controller Directive
                SmartHomeResponse response = await Alexa.Instance.HandleSceneControllerDirectiveAsync(request, deviceItem, customer).ConfigureAwait(false);

                return response;
            }

            // Could not handle Directive
            return Alexa.Instance.CreateErrorEvent(request, ErrorTypes.INVALID_DIRECTIVE, "Could not understand directive");
        }

        public async Task<ActionResponse> ActionHandlerAsync(ActionRequest request, string token)
        {
            // Check if action is SYNC
            if (request.Inputs[0].Intent == IntentNames.SYNC)
            {
                // If token is empty, return error.
                if (string.IsNullOrEmpty(token))
                {
                    Logger.Instance.Debug("Customer empty");
                    ActionResponse actionResponse = new ActionResponse(request);
                    ((SyncPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((SyncPayload)actionResponse.Payload).DebugString = "Customer empty";

                    return actionResponse;
                }

                // Try to find token in token cache
                Logger.Instance.Debug("Search customer: {0}", token);

                string customerId = Cache.Instance.CheckToken(token);

                if (customerId != null && customerId?.Length > 1)
                {
                    Logger.Instance.Debug("Found customer in cache.");
                }
                else
                {
                    // Query oauth for user
                    Logger.Instance.Debug("Customer not found in cache, querying oauth");
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                    if (customerId.Length <= 1)
                    {
                        // Getting customer id from oauth token failed
                        Logger.Instance.Debug("Could not get customer id");
                        ActionResponse actionResponse = new ActionResponse(request);
                        ((SyncPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                        ((SyncPayload)actionResponse.Payload).DebugString = "Could not get customer id";

                        return actionResponse;
                    }

                    // Save token in cache
                    Cache.Instance.StoreToken(token, customerId);
                }

                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    // Customer not in database, create new user and save to database
                    customer = new Customer
                    {
                        IsGoogleAssistant = true,
                        CustomerId = customerId,
                    };
                    Logger.Instance.Debug("Saving new customer in db");
                    PostgreSQL.Instance.SaveCustomer(customer);

                    ActionResponse actionResponse = Assistant.Instance.HandleSyncIntent(request, customer);
                    return actionResponse;
                }

                // Mark as Assistant user
                customer.IsGoogleAssistant = true;

                // Check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                // If user could not be found, return directly
                if (!isValidUri)
                {
                    ActionResponse actionResponse = Assistant.Instance.HandleSyncIntent(request, customer);
                    return actionResponse;
                }

                // Try to query for devices if list is old
                ConnectionState connection;
                if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(60) < DateTime.UtcNow
                    || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                {
                    connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), false).ConfigureAwait(false);

                    customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices = DateTime.UtcNow;
                }

                // save customer
                PostgreSQL.Instance.SaveCustomer(customer);

                // Submit device states to homegraph
                connection = await FritzBoxViaRabbitMQ.Instance.HomeGraphAsync(customer).ConfigureAwait(false);

                // Get response for Google Assistant
                ActionResponse response = Assistant.Instance.HandleSyncIntent(request, customer);

                return response;
            }
            // Check if action is QUERY
            else if (request.Inputs[0].Intent == IntentNames.QUERY)
            {
                // If token is empty, return error.
                if (string.IsNullOrEmpty(token))
                {
                    Logger.Instance.Debug("Customer empty");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer empty";

                    return actionResponse;
                }

                string customerId;
                // Check if cookie with customer ID was transmitted        
                if (((QueryPayload)request.Inputs[0].Payload).Devices[0].CustomData.ContainsKey("customerId"))
                {
                    customerId = (string)((QueryPayload)request.Inputs[0].Payload).Devices[0].CustomData["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer not found";

                    return actionResponse;
                }

                // Mark as Assistant user
                customer.IsGoogleAssistant = true;

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Customer not found or no valid uri");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.DEVICEOFFLINE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Connection failed. Invalid URI.";

                    return actionResponse;
                }

                // Get connections
                List<ConnectionNumber> connections = new List<ConnectionNumber>();
                foreach (QueryDevice queryDevice in ((QueryPayload)request.Inputs[0].Payload).Devices)
                {
                    Logger.Instance.Debug("Search endpoint: {0}", queryDevice.Id);
                    Device deviceItem = customer.DeviceList.Find(device => device.Identifier == queryDevice.Id);
                    if (deviceItem != null && !connections.Contains(deviceItem.ConnectionNumber))
                    {
                        connections.Add(deviceItem.ConnectionNumber);
                    }
                }
                ConnectionNumber connectionNumber;
                if (connections.Count >= 1)
                {
                    connectionNumber = connections[0];
                }
                else
                {
                    connectionNumber = ConnectionNumber.DEFAULT;
                }

                // Try to query for devices
                // Get cached device list if cache is not outdated (<10min)
                // Else query fritz!box
                // Also query if last try was not successfull
                if (customer.GetConnection(connectionNumber).LastUpdatedDevices.AddMinutes(10) < DateTime.UtcNow
                    || customer.GetConnection(connectionNumber).LastConnectionState != ConnectionState.OK
                    || connections.Count <= 0)
                {
                    ConnectionState connection;

                    // If last update was long ago or failed, use direct connection to prevent displaying wrong temperatures
                    // Only update directly if only one connection is used, else it will take too long anyways
                    if (
                        customer.GetConnection(connectionNumber).LastUpdatedDevices.AddMinutes(90) < DateTime.UtcNow
                        && customer.GetConnection(connectionNumber).LastConnectionState == ConnectionState.OK
                        && connections.Count <= 1
                       )
                    {
                        Logger.Instance.Debug("Update outdated temperatures");
                        connection = await FritzBox.Instance.GetDevicesAsync(customer, customer.GetConnection(connectionNumber), false).ConfigureAwait(false);

                        // If direct connection fails, try in background to get longer timeouts.
                        if (connection == ConnectionState.TIMEOUT)
                        {
                            Logger.Instance.Debug("Direct connection failed, retrying");
                            await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(connectionNumber), false).ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(connectionNumber), false).ConfigureAwait(false);
                    }
                    
                    customer.GetConnection(connectionNumber).LastUpdatedDevices = DateTime.UtcNow;
                    PostgreSQL.Instance.SaveCustomer(customer);

                    if (connection != ConnectionState.OK)
                    {
                        Logger.Instance.Debug("Connection failed.");
                        ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                        ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        ((ActionErrorPayload)actionResponse.Payload).DebugString = "Connection failed.";

                        return actionResponse;
                    }
                }

                // Start creating action response
                ActionResponse response = new ActionResponse(request);

                // Get devices
                foreach (QueryDevice queryDevice in ((QueryPayload)request.Inputs[0].Payload).Devices)
                {
                    Logger.Instance.Debug("Search endpoint: {0}", queryDevice.Id);
                    Device deviceItem = customer.DeviceList.Find(device => device.Identifier == queryDevice.Id);

                    // If device does not exist, add error
                    if (deviceItem == null)
                    {
                        Logger.Instance.Debug("Device not found");

                        // Create state map
                        Dictionary<string, object> stateMap = new Dictionary<string, object>
                        {
                            { "errorCode", ErrorCodes.DEVICENOTFOUND },
                            { "debugString", "Device not found" }
                        };

                        // Add device to response list
                        ((QueryResponsePayload)response.Payload).Devices.Add(queryDevice.Id, stateMap);
                    }
                    else
                    {
                        // Device found, get state map
                        Logger.Instance.Debug("Device found");
                        Dictionary<string, object> stateMap = Assistant.Instance.HandleQueryIntent(customer, deviceItem);

                        // Add device to response list
                        ((QueryResponsePayload)response.Payload).Devices.Add(queryDevice.Id, stateMap);
                    }
                }

                return response;
            }
            // Check if action is EXECUTE
            else if (request.Inputs[0].Intent == IntentNames.EXECUTE)
            {
                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.access|#service:{2},request",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, "assistant");

                // If token is empty, return error.
                if (string.IsNullOrEmpty(token))
                {
                    Logger.Instance.Debug("Customer empty");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer empty";

                    return actionResponse;
                }

                string customerId;
                // Check if cookie with customer ID was transmitted
                if (((ExecutePayload)request.Inputs[0].Payload).Commands[0].Devices[0].CustomData.ContainsKey("customerId"))
                {
                    customerId = (string)((ExecutePayload)request.Inputs[0].Payload).Commands[0].Devices[0].CustomData["customerId"];
                    Logger.Instance.Log("Cookie with customer Id found {0}", customerId);
                }
                else
                {
                    Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                    customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);
                }

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer not found";

                    return actionResponse;
                }

                // Mark as Assistant user
                customer.IsGoogleAssistant = true;

                // check for valid FRITZ!Box data. If no URI available, fail faster
                bool isValidUri = Uri.IsWellFormedUriString(customer.GetConnection(ConnectionNumber.DEFAULT).FritzBoxUrl, UriKind.Absolute);

                if (!isValidUri)
                {
                    Logger.Instance.Debug("Customer not found or no valid uri");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.DEVICEOFFLINE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Connection failed. Invalid URI.";

                    return actionResponse;
                }

                // Start creating action response
                ActionResponse response = new ActionResponse(request);
                ((ExecuteResponsePayload)response.Payload).Commands = new List<ResponseCommand>();

                // List to save responseCommand together with execution
                List<Tuple<ResponseCommand, Execution, Device>> commandList = new List<Tuple<ResponseCommand, Execution, Device>>();

                // Get connections
                List<ConnectionNumber> connections = new List<ConnectionNumber>();
                foreach (Command command in ((ExecutePayload)request.Inputs[0].Payload).Commands)
                {
                    foreach (ExecuteDevice executeDevice in command.Devices)
                    {
                        Device deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));
                        if (deviceItem != null && !connections.Contains(deviceItem.ConnectionNumber))
                        {
                            connections.Add(deviceItem.ConnectionNumber);
                        }
                    }
                }

                // Run through all commands
                foreach (Command command in ((ExecutePayload)request.Inputs[0].Payload).Commands)
                {
                    // Run through all devices
                    foreach (ExecuteDevice executeDevice in command.Devices)
                    {
                        // Create response command for device
                        ResponseCommand responseCommand = new ResponseCommand
                        {
                            Ids = new List<string>
                            {
                                executeDevice.Id
                            }
                        };

                        Logger.Instance.Debug("Looking for device: {0}", executeDevice.Id);

                        // Get device
                        Device deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));

                        // If device does not exist, try to update list
                        if (deviceItem == null)
                        {
                            Logger.Instance.Debug("Device not found, updating list");

                            // Try to update device list if it is old
                            if (customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                                || customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState != ConnectionState.OK)
                            {
                                ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(ConnectionNumber.DEFAULT), true).ConfigureAwait(false);
                                
                                customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState = connection;
                                customer.GetConnection(ConnectionNumber.DEFAULT).LastUpdatedDevices = DateTime.UtcNow;
                            }

                            deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));

                            // If device still does not exist, add error to response
                            if (deviceItem == null)
                            {
                                Logger.Instance.Debug("Device not found");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.DEVICENOTFOUND;
                                responseCommand.DebugString = "Device not found";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                        }
                        // If device is offline and on low battery, send error
                        else if (!deviceItem.Present && deviceItem.BatteryLow)
                        {
                            Logger.Instance.Debug("Device is offline on low power, updating list");

                            // Try to update device list if it is old
                            if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                                || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                            {
                                ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);
 
                                customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState = connection;
                                customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices = DateTime.UtcNow;
                            }

                            deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));

                            // If device still is not present, add error to response
                            if (deviceItem == null)
                            {
                                Logger.Instance.Debug("Device not found");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.DEVICENOTFOUND;
                                responseCommand.DebugString = "Device not found";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                            else if (!deviceItem.Present)
                            {
                                Logger.Instance.Debug("Device not reachable");
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.LOWBATTERY;
                                responseCommand.DebugString = "Device is on low battery";
                                responseCommand.States = new Dictionary<string, object>
                                {
                                    { States.ONLINE, false }
                                };
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                        }
                        // If device is offline, send error
                        else if (!deviceItem.Present)
                        {
                            Logger.Instance.Debug("Device is offline, updating list");

                            // Try to update device list if it is old
                            if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                                || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                            {
                                ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);

                                customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState = connection;
                                customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices = DateTime.UtcNow;
                            }

                            deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));
                            // If device sill is not present, add error to response
                            if (deviceItem == null)
                            {
                                Logger.Instance.Debug("Device not found");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.DEVICENOTFOUND;
                                responseCommand.DebugString = "Device not found";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                            else if (!deviceItem.Present)
                            {
                                Logger.Instance.Debug("Device not reachable");
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Device offline";
                                responseCommand.States = new Dictionary<string, object>
                                {
                                    { States.ONLINE, false }
                                };
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                        }
                        // If device is locked, send error
                        else if (deviceItem.Locked)
                        {
                            Logger.Instance.Debug("Device is locked, updating list");

                            // Try to update device list if it is old
                            if (customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices.AddMinutes(5) < DateTime.UtcNow
                                || customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState != ConnectionState.OK)
                            {
                                ConnectionState connection = await FritzBoxViaRabbitMQ.Instance.GetDevicesAsync(customer, customer.GetConnection(deviceItem.ConnectionNumber), true).ConfigureAwait(false);

                                customer.GetConnection(deviceItem.ConnectionNumber).LastConnectionState = connection;
                                customer.GetConnection(deviceItem.ConnectionNumber).LastUpdatedDevices = DateTime.UtcNow;
                            }

                            deviceItem = customer.DeviceList.Find(device => device.Identifier == executeDevice.Id.Replace("-virtual", ""));
                            // If device sill is locked, add error to response
                            if (deviceItem == null)
                            {
                                Logger.Instance.Debug("Device not found");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.DEVICENOTFOUND;
                                responseCommand.DebugString = "Device not found";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                            else if (deviceItem.Locked)
                            {
                                Logger.Instance.Debug("Device locked");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.DIRECTRESPONSEONLYUNREACHABLE;
                                responseCommand.DebugString = "Device locked";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                continue;
                            }
                        }

                        // Device OK                       
                        Logger.Instance.Debug("Device OK");

                        // If device is power switch
                        if (deviceItem.IsDeviceClass(DeviceClass.PowerSwitch))
                        {
                            // Check for correct device commands and parameters
                            // Only one command expected for power switch (OnOff)
                            if (command.Execution[0].Command == Commands.ONOFF
                                && command.Execution[0].Params.ContainsKey(Params.ON))
                            {
                                // Add command to List
                                commandList.Add(new Tuple<ResponseCommand, Execution, Device>(responseCommand, command.Execution[0], deviceItem));
                            }
                            // Else command is invalid
                            else
                            {
                                Logger.Instance.Debug("Command not supported");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                                responseCommand.DebugString = "Command not supported";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                            }
                        }
                        // If device is thermostat
                        else if (deviceItem.IsDeviceClass(DeviceClass.Thermostat))
                        {
                            // Walk through all executions
                            Execution thermostatExecution = null;
                            foreach (Execution execution in command.Execution)
                            {
                                // Check if command is setpoint, Setpoint always has priority
                                if (execution.Command == Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                                {
                                    // Save execution
                                    thermostatExecution = execution;
                                }
                                // Else check if command is mode
                                else if (execution.Command == Commands.THERMOSTAT_SETMODE)
                                {
                                    // Only save execution if thermostatExecution is not Setpoint
                                    if (thermostatExecution?.Command != Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                                    {
                                        thermostatExecution = execution;
                                    }
                                }
                                // Else check if command is onOff
                                else if (execution.Command == Commands.ONOFF)
                                {
                                    // Only save execution if thermostatExecution is not Setpoint
                                    if (thermostatExecution?.Command != Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                                    {
                                        thermostatExecution = execution;
                                    }
                                }
                            }

                            // if no execution was saved, command is not supported
                            if (thermostatExecution == null)
                            {
                                Logger.Instance.Debug("Command not supported");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                                responseCommand.DebugString = "Command not supported";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                            }
                            // else if command is setpoint
                            else if (thermostatExecution.Command == Commands.THERMOSTAT_TEMPERATURE_SETPOINT
                                        && thermostatExecution.Params.ContainsKey(Params.TEMPERATURE_SETPOINT))
                            {
                                // Get setpoint
                                thermostatExecution.Params.TryGetValue(Params.TEMPERATURE_SETPOINT, out object value);
                                double setpoint = (value == null) ? 0.0 : Convert.ToDouble(value);

                                // Round temperature setpoint to the nearest 0.5C
                                setpoint = Math.Round(setpoint * 2.0) / 2.0;

                                // Check if setpoint is between 8C and 28C, else it is not supported
                                if (setpoint >= 8.0 && setpoint <= 28.0)
                                {
                                    // Add setpoint back to execution
                                    thermostatExecution.Params[Params.TEMPERATURE_SETPOINT] = setpoint;

                                    // Add command to List
                                    commandList.Add(new Tuple<ResponseCommand, Execution, Device>(responseCommand, thermostatExecution, deviceItem));
                                }
                                else
                                {
                                    // Else send error
                                    Logger.Instance.Debug("Temperature setpoint out of range");
                                    responseCommand.Status = Status.ERROR;
                                    responseCommand.ErrorCode = ErrorCodes.VALUEOUTOFRANGE;
                                    responseCommand.DebugString = "Temperature setpoint out of range";
                                    ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                }
                            }
                            else if (thermostatExecution.Command == Commands.THERMOSTAT_SETMODE
                                        && thermostatExecution.Params.ContainsKey(Params.THERMOSTAT_MODE))
                            {
                                // Get mode
                                thermostatExecution.Params.TryGetValue(Params.THERMOSTAT_MODE, out object mode);
                                string thermostatMode = (mode == null) ? string.Empty : Convert.ToString(mode);

                                // Check if mode is a supported mode
                                if (thermostatMode == Modes.ON
                                    || thermostatMode == Modes.HEAT
                                    || thermostatMode == Modes.COOL
                                    || thermostatMode == Modes.ECO
                                    || thermostatMode == Modes.OFF)
                                {
                                    // Add command to List
                                    commandList.Add(new Tuple<ResponseCommand, Execution, Device>(responseCommand, thermostatExecution, deviceItem));
                                }
                                else
                                {
                                    // Else send error
                                    Logger.Instance.Debug("Not a valid thermostat mode");
                                    responseCommand.Status = Status.ERROR;
                                    responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                                    responseCommand.DebugString = "Not a valid thermostat mode";
                                    ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                                }
                            }
                            else if (thermostatExecution.Command == Commands.ONOFF
                                        && thermostatExecution.Params.ContainsKey(Params.ON))
                            {
                                // Add command to List
                                commandList.Add(new Tuple<ResponseCommand, Execution, Device>(responseCommand, thermostatExecution, deviceItem));
                            }
                            // else it is not supported
                            else
                            {
                                Logger.Instance.Debug("Command not supported");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                                responseCommand.DebugString = "Command not supported";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                            }
                        }
                        // If device is template
                        else if (deviceItem.IsDeviceClass(DeviceClass.Template))
                        {
                            // Check for correct device commands and parameters
                            // Only one command expected for template (deactivate)
                            if (command.Execution[0].Command == Commands.ACTIVATE_SCENE
                                && command.Execution[0].Params.ContainsKey(Params.DEACTIVATE))
                            {
                                // Add command to List
                                commandList.Add(new Tuple<ResponseCommand, Execution, Device>(responseCommand, command.Execution[0], deviceItem));
                            }
                            // Else command is invalid
                            else
                            {
                                Logger.Instance.Debug("Command not supported");
                                responseCommand.Status = Status.ERROR;
                                responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                                responseCommand.DebugString = "Command not supported";
                                ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                            }
                        }
                        // Unsupported device type
                        else
                        {
                            Logger.Instance.Debug("Device not supported");
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                            responseCommand.DebugString = "Device not supported";
                            ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                        }
                    }
                }

                // Hand off to Assistant functions to handle FRITZ!Box access
                List<ResponseCommand> responseCommandList = await Assistant.Instance.HandleExecuteIntentAsync(commandList, customer).ConfigureAwait(false);

                // Add commands to action response payload
                foreach (ResponseCommand responseCommand in responseCommandList)
                {
                    ((ExecuteResponsePayload)response.Payload).Commands.Add(responseCommand);
                }

                // Return response
                return response;
            }
            // Check if action is DISCONNECT
            else if (request.Inputs[0].Intent == IntentNames.DISCONNECT)
            {
                Logger.Instance.Debug("Disconnecting requested");

                // If token is empty, return error.
                if (string.IsNullOrEmpty(token))
                {
                    Logger.Instance.Debug("Customer empty");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer empty";

                    return actionResponse;
                }

                Logger.Instance.Debug("Get customer from OAuth: {0}", token);
                string customerId = await OAuth.Instance.GetCustomerIdAsync(token).ConfigureAwait(false);

                // Query database for user
                Customer customer = PostgreSQL.Instance.GetCustomer(customerId);

                // If user could not be found, return error
                if (customer == null || customer?.CustomerId.Length <= 1)
                {
                    Logger.Instance.Debug("Customer not found");
                    ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                    ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.AUTHFAILURE;
                    ((ActionErrorPayload)actionResponse.Payload).DebugString = "Customer not found";

                    return actionResponse;
                }

                // Remove as Assistant user
                customer.IsGoogleAssistant = false;

                // Save customer data
                Logger.Instance.Debug("Saving customer token in db");
                PostgreSQL.Instance.SaveCustomer(customer);

                return null;
            }
            else
            {
                // No valid Intent
                ActionResponse actionResponse = ActionResponse.GetErrorResponse(request);
                ((ActionErrorPayload)actionResponse.Payload).ErrorCode = ErrorCodes.NOTSUPPORTED;
                ((ActionErrorPayload)actionResponse.Payload).DebugString = "No valid intent";

                return actionResponse;
            }
        }
    }
}