﻿namespace FBSmartHome
{
    using FBSmartHome.GoogleAssistant.JsonObjects;
    using FBSmartHome.GoogleAssistant.Payloads.Response;
    using FBSmartHome.GoogleAssistant.Request;
    using FBSmartHome.GoogleAssistant.Response;
    using FBSmartHome.GoogleAssistant.Types;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public sealed class Assistant
    {
        public static readonly Assistant Instance = new Assistant();
        private readonly IFritzBox FritzBoxInstance;

        static Assistant()
        {
        }

        private Assistant()
        {
            if (Environment.GetEnvironmentVariable("USE_SNS") == "true")
            {
                FritzBoxInstance = FritzBoxViaRabbitMQ.Instance;
            }
            else
            {
                FritzBoxInstance = FritzBox.Instance;
            }
        }

        // Handle Google Assistant SYNC Intent
        // Reply with a list of found devices
        public ActionResponse HandleSyncIntent(ActionRequest request, Customer customer)
        {
            Logger.Instance.Debug("Handling Sync Intent");
            ActionResponse response = new ActionResponse(request);
            ((SyncPayload)response.Payload).AgentUserId = customer.CustomerId;

            foreach (Device device in customer.DeviceList)
            {
                // Create new device
                // Add Identifier
                SyncResponseDevice responseDevice = new SyncResponseDevice
                {
                    Id = device.Identifier,
                    WillReportState = false
                };

                // Add device name
                responseDevice.Name = new DeviceName
                {
                    Name = device.Name
                };

                // Add device info, not for templates
                if (!device.IsDeviceClass(DeviceClass.Template))
                {
                    responseDevice.DeviceInfo = new DeviceInfo
                    {
                        Manufacturer = device.Manufacturer,
                        Model = device.ProductName,
                        HwVersion = "",
                        SwVersion = device.Firmware
                    };
                }

                // Add cookie
                responseDevice.CustomData = new Dictionary<string, object>
                {
                    { "customerId", customer.CustomerId }
                };

                // If device is Thermostat, 
                // add device type: Thermostat
                // add device traits: TemperatureSetting 
                // add device attributes: availableThermostatModes, thermostatTemperatureUnit
                if (device.IsDeviceClass(DeviceClass.Thermostat))
                {
                    responseDevice.Type = DeviceType.THERMOSTAT;
                    responseDevice.Traits = new List<string>
                    {
                        DeviceTraits.TEMPERATURESETTING,
                        DeviceTraits.ONOFF
                    };

                    responseDevice.Attributes = new Dictionary<string, object>
                    {
                        { Attributes.THERMOSTAT_MODES, string.Join(",", new string[] { Modes.OFF, Modes.HEAT, Modes.ECO, Modes.ON }) },
                        { Attributes.TEMPERATURE_UNIT, TemperatureUnit.C }
                    };

                    // Add device to response
                    ((SyncPayload)response.Payload).Devices.Add(responseDevice);
                }
                // Else if device is Powerswitch, 
                // add device type: Outlet
                // add device traits: OnOff, TemperatureSetting with attribute queryOnlyTemperatureSetting if temperature sensor
                else if (device.IsDeviceClass(DeviceClass.PowerSwitch))
                {
                    responseDevice.Type = DeviceType.OUTLET;
                    responseDevice.Traits = new List<string>
                    {
                        DeviceTraits.ONOFF
                    };

                    // If device is temperature sensor, add TemperatureSettings trait with attribute queryOnlyTemperatureSetting
                    if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
                    {
                        responseDevice.Traits.Add(DeviceTraits.TEMPERATURESETTING);
                        responseDevice.Attributes = new Dictionary<string, object>
                        {
                            { Attributes.QUERYONLY_TEMPERATURESETTING, true }
                        };
                    }

                    // Add device to response
                    ((SyncPayload)response.Payload).Devices.Add(responseDevice);
                }
                // Else if device is Template
                // add device type: Scene
                // add device traits: Scene
                // add device attributes: sceneReversible
                else if (device.IsDeviceClass(DeviceClass.Template))
                {
                    responseDevice.Type = DeviceType.SCENE;
                    responseDevice.Traits = new List<string>
                    {
                        DeviceTraits.SCENE
                    };
                    responseDevice.Attributes = new Dictionary<string, object>
                    {
                        { Attributes.SCENE_REVERSIBLE, false }
                    };

                    // Add device to response
                    ((SyncPayload)response.Payload).Devices.Add(responseDevice);
                }

                /*
                // If device is in the virtual device list, create an outlet for it, disable for now
                if (customer.VirtualDevices.Any(listItem => listItem.Equals(device.Identifier)) && false)
                {
                    // Create new device
                    // Add Identifier
                    responseDevice = new SyncResponseDevice
                    {
                        Id = device.Identifier + "-virtual",
                        WillReportState = false
                    };

                    // Add device name
                    responseDevice.Name = new DeviceName
                    {
                        Name = device.Name + " VS"
                    };

                    // Add device info
                    responseDevice.DeviceInfo = new DeviceInfo
                    {
                        Manufacturer = device.Manufacturer,
                        Model = device.ProductName + " virtueller Schalter",
                        HwVersion = "",
                        SwVersion = device.Firmware
                    };

                    // Add cookie
                    responseDevice.CustomData = new Dictionary<string, object>
                    {
                        { "customerId", customer.CustomerId }
                    };

                    // Add outlet type and traits
                    responseDevice.Type = DeviceType.OUTLET;
                    responseDevice.Traits = new List<string>
                    {
                        DeviceTraits.ONOFF
                    };

                    // Add device to response
                    ((SyncPayload)response.Payload).Devices.Add(responseDevice);
                }
                */
            }

            return response;
        }

        // Handle Google Assistant QUERY Intent
        // Reply with a device for response device list
        public Dictionary<string, object> HandleQueryIntent(Customer customer, Device device)
        {
            Dictionary<string, object> stateMap = new Dictionary<string, object>();

            if (device.IsDeviceClass(DeviceClass.PowerSwitch))
            {
                // Add OnOff state
                stateMap.Add(States.ON, device.SwitchedOn);

                // If device has temperature sensor, add ambient temperature
                if (device.IsDeviceClass(DeviceClass.TemperatureSensor))
                {
                    stateMap.Add(States.THERMOSTAT_TEMPERATURE, device.Temperature);
                }

                // Add online state
                stateMap.Add(States.ONLINE, device.Present);
            }
            else if (device.IsDeviceClass(DeviceClass.Thermostat))
            {
                // Add ambient temperature value
                // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                if (device.IsGroup)
                {
                    if (device.GroupMembers.Count > 0)
                    {
                        Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                        if (firstGroupDevice != null)
                        {
                            stateMap.Add(States.THERMOSTAT_TEMPERATURE, firstGroupDevice.Temperature);
                        }
                        else
                        {
                            stateMap.Add(States.THERMOSTAT_TEMPERATURE, device.TemperatureActualValue);
                        }
                    }
                    else
                    {
                        stateMap.Add(States.THERMOSTAT_TEMPERATURE, device.TemperatureActualValue);
                    }
                }
                else
                {
                    stateMap.Add(States.THERMOSTAT_TEMPERATURE, device.Temperature);
                }

                // Add setpoint
                double temperatureSetpoint;
                if (device.TemperatureSetpoint == (double)Temperature.ON)
                {
                    temperatureSetpoint = 28;
                }
                else if (device.TemperatureSetpoint == (double)Temperature.OFF)
                {
                    temperatureSetpoint = 8;
                }
                else
                {
                    temperatureSetpoint = device.TemperatureSetpoint;
                }

                stateMap.Add(States.THERMOSTAT_SETPOINT, temperatureSetpoint);

                // Add thermostat mode
                if (device.TemperatureSetpoint == (double)Temperature.OFF)
                {
                    stateMap.Add(States.THERMOSTAT_MODE, Modes.OFF);
                }
                else if (device.TemperatureSetpoint == (double)Temperature.ON)
                {
                    stateMap.Add(States.THERMOSTAT_MODE, Modes.ON);
                }
                else if (device.TemperatureSetpoint <= device.TemperatureSetback
                    && device.TemperatureSetback < device.TemperatureComfort
                    && device.TemperatureSetback != (double)Temperature.OFF
                    && device.TemperatureSetback != (double)Temperature.ON)
                {
                    stateMap.Add(States.THERMOSTAT_MODE, Modes.ECO);
                }
                else
                {
                    stateMap.Add(States.THERMOSTAT_MODE, Modes.HEAT);
                }

                bool onOffState = device.TemperatureSetpoint != (double)Temperature.OFF
                    && device.TemperatureSetpoint != device.TemperatureOff;

                // Add OnOff state
                stateMap.Add(States.ON, onOffState);

                // Add online state
                stateMap.Add(States.ONLINE, device.Present);
            }
            else if (device.IsDeviceClass(DeviceClass.Template))
            {
                // Add online state
                stateMap.Add(States.ONLINE, true);
            }

            return stateMap;
        }

        // Handle Google Assistant EXECUTE Intent
        // Reply with a response command list
        public async Task<List<ResponseCommand>> HandleExecuteIntentAsync(List<Tuple<ResponseCommand, Execution, Device>> commandList, Customer customer)
        {
            List<ResponseCommand> responseCommandList = new List<ResponseCommand>();

            // If list contains no command, return empty list
            if (commandList.Count <= 0)
            {
                Logger.Instance.Debug("Empty command list");

                return responseCommandList;
            }
            // If List only contains one device, handle it like Alexa
            else if (commandList.Count == 1)
            {
                // Get data
                ResponseCommand responseCommand = commandList[0].Item1;
                Execution execution = commandList[0].Item2;
                Device device = commandList[0].Item3;

                // Handle OnOff command
                if (execution.Command == Commands.ONOFF)
                {
                    ConnectionState connection;

                    double onTemperature = device.TemperatureComfort;
                    double offTemperature = (double)Temperature.OFF;

                    // If device is a thermostat, hand over to thermostat control
                    if (device.IsDeviceClass(DeviceClass.Thermostat))
                    {
                        // check for user set temperature. If not set, use comfort temperature
                        if ((device.TemperatureOn >= (double)Temperature.MINIMUM_VALUE && device.TemperatureOn <= (double)Temperature.MAXIMUM_VALUE)
                            || (device.TemperatureOn >= (double)Temperature.NOCHANGE && device.TemperatureOn <= (double)Temperature.COMFORT))
                        {
                            if (device.TemperatureOn == (double)Temperature.NOCHANGE)
                            {
                                onTemperature = device.TemperatureSetpoint;
                            }
                            else if (device.TemperatureOn == (double)Temperature.ECO)
                            {
                                onTemperature = device.TemperatureSetback;
                            }
                            else if (device.TemperatureOn == (double)Temperature.COMFORT)
                            {
                                onTemperature = device.TemperatureComfort;
                            }
                            else
                            {
                                onTemperature = device.TemperatureOn;
                            }
                        }

                        // check for user set temperature. If not set, set thermostat off
                        if ((device.TemperatureOff >= (double)Temperature.MINIMUM_VALUE && device.TemperatureOff <= (double)Temperature.MAXIMUM_VALUE)
                            || (device.TemperatureOff >= (double)Temperature.NOCHANGE && device.TemperatureOff <= (double)Temperature.COMFORT))
                        {
                            if (device.TemperatureOff == (double)Temperature.NOCHANGE)
                            {
                                offTemperature = device.TemperatureSetpoint;
                            }
                            else if (device.TemperatureOff == (double)Temperature.ECO)
                            {
                                offTemperature = device.TemperatureSetback;
                            }
                            else if (device.TemperatureOff == (double)Temperature.COMFORT)
                            {
                                offTemperature = device.TemperatureComfort;
                            }
                            else
                            {
                                offTemperature = device.TemperatureOff;
                            }
                        }

                        double temperature;
                        if ((bool)execution.Params[Params.ON])
                        {
                            temperature = onTemperature;
                        }
                        else
                        {
                            temperature = offTemperature;
                        }

                        // If no change is requested, just update devices
                        if ((bool)execution.Params[Params.ON] && device.TemperatureOn == (double)Temperature.NOCHANGE)
                        {
                            connection = await FritzBoxInstance.GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), true).ConfigureAwait(false);
                        }
                        else if (!(bool)execution.Params[Params.ON] && device.TemperatureOff == (double)Temperature.NOCHANGE)
                        {
                            connection = await FritzBoxInstance.GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), true).ConfigureAwait(false);
                        }
                        else
                        {
                            connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, temperature, customer).ConfigureAwait(false);
                        }
                    }
                    // device is outlet
                    else
                    {
                        if ((bool)execution.Params[Params.ON])
                        {
                            connection = await FritzBoxInstance.HandleTurnOnAsync(device, customer).ConfigureAwait(false);
                        }
                        else
                        {
                            connection = await FritzBoxInstance.HandleTurnOffAsync(device, customer).ConfigureAwait(false);
                        }
                    }

                    if (connection == ConnectionState.OK)
                    {
                        // Add response SUCCESS
                        responseCommand.Status = Status.SUCCESS;

                        // Add new device state
                        responseCommand.States = new Dictionary<string, object>
                        {
                            { States.ON, (bool)execution.Params[Params.ON] },
                            { States.ONLINE, true }
                        };

                        // If device is on low battery, add exception code
                        if(device.BatteryLow)
                        {
                            responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                        }

                        // Add command to return list
                        responseCommandList.Add(responseCommand);
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Device is unreachable
                        responseCommand.Status = Status.ERROR;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Device is unreachable";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Device is unreachable");
                    }
                    else
                    {
                        // Connection failed
                        responseCommand.Status = Status.ERROR;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Connection failed";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Connection failed");
                    }
                }
                // Handle ActivateScene command
                if (execution.Command == Commands.ACTIVATE_SCENE)
                {
                    ConnectionState  connection = await FritzBoxInstance.HandleApplyTemplateAsync(device, customer).ConfigureAwait(false);

                    if (connection == ConnectionState.OK)
                    {
                        // Add response SUCCESS
                        responseCommand.Status = Status.SUCCESS;

                        // Add new device state
                        responseCommand.States = new Dictionary<string, object>();

                        // Add command to return list
                        responseCommandList.Add(responseCommand);
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Device is unreachable
                        responseCommand.Status = Status.ERROR;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Device is unreachable";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Device is unreachable");
                    }
                    else
                    {
                        // Connection failed
                        responseCommand.Status = Status.ERROR;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Connection failed";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Connection failed");
                    }
                }
                // Handle setpoint command
                else if (execution.Command == Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                {
                    // If last connection failed, use direct connection
                    ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device,
                            (double)execution.Params[Params.TEMPERATURE_SETPOINT], customer).ConfigureAwait(false);

                    if (connection == ConnectionState.OK)
                    {
                        // Add response SUCCESS
                        responseCommand.Status = Status.SUCCESS;

                        // Get ambient temperature value
                        // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                        double ambientTemperature = 0;
                        if (device.IsGroup)
                        {
                            if (device.GroupMembers.Count > 0)
                            {
                                Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                                if (firstGroupDevice != null)
                                {
                                    ambientTemperature = firstGroupDevice.Temperature;
                                }
                                else
                                {
                                    ambientTemperature = device.TemperatureActualValue;
                                }
                            }
                            else
                            {
                                ambientTemperature = device.TemperatureActualValue;
                            }
                        }
                        else
                        {
                            ambientTemperature = device.Temperature;
                        }

                        // Add new device state
                        responseCommand.States = new Dictionary<string, object>
                        {
                            { States.THERMOSTAT_SETPOINT, (double)execution.Params[Params.TEMPERATURE_SETPOINT] },
                            { States.THERMOSTAT_TEMPERATURE, ambientTemperature },
                            { States.ONLINE, true }
                        };

                        if ((double)execution.Params[Params.TEMPERATURE_SETPOINT] <= device.TemperatureSetback
                                && device.TemperatureSetback != (double)Temperature.OFF
                                && device.TemperatureSetback != (double)Temperature.ON)
                        {
                            responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.ECO);
                        }
                        else
                        {
                            responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.HEAT);
                        }

                        // If device is on low battery, add exception code
                        if (device.BatteryLow)
                        {
                            responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                        }

                        // Add command to return list
                        responseCommandList.Add(responseCommand);
                    }
                    else if (connection == ConnectionState.FAILED)
                    {
                        // Device is unreachable
                        responseCommand.Status = Status.OFFLINE;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Device is unreachable";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Device is unreachable");
                    }
                    else
                    {
                        // Connection failed
                        responseCommand.Status = Status.OFFLINE;
                        responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                        responseCommand.DebugString = "Connection failed";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Connection failed");
                    }
                }
                // Handle setmode command
                else if (execution.Command == Commands.THERMOSTAT_SETMODE)
                {
                    // Get requested mode
                    string thermostatMode = execution.Params[Params.THERMOSTAT_MODE].ToString();

                    if (thermostatMode == Modes.ON || thermostatMode == Modes.HEAT)
                    {
                        // If last connection failed, use direct connection
                        ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, device.TemperatureComfort, customer).ConfigureAwait(false);

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>
                        {
                            { States.THERMOSTAT_TEMPERATURE, device.Temperature },
                            { States.ONLINE, true }
                        };

                            double temperatureSetpoint;
                            if (device.TemperatureComfort == (double)Temperature.ON)
                            {
                                temperatureSetpoint = 28;
                            }
                            else if (device.TemperatureComfort == (double)Temperature.OFF)
                            {
                                temperatureSetpoint = 8;
                            }
                            else
                            {
                                temperatureSetpoint = device.TemperatureComfort;
                            }

                            responseCommand.States.Add(States.THERMOSTAT_SETPOINT, temperatureSetpoint);

                            string setThermostatMode;
                            if (device.TemperatureComfort == (double)Temperature.OFF)
                            {
                                setThermostatMode = Modes.OFF;
                            }
                            else
                            {
                                setThermostatMode = Modes.HEAT;
                            }

                            responseCommand.States.Add(States.THERMOSTAT_MODE, setThermostatMode);

                            // If device is on low battery, add exception code
                            if (device.BatteryLow)
                            {
                                responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                            }

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                    else if (thermostatMode == Modes.COOL || thermostatMode == Modes.ECO)
                    {
                        // If last connection failed, use direct connection
                        ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, device.TemperatureSetback, customer).ConfigureAwait(false);

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>
                            {
                                { States.THERMOSTAT_TEMPERATURE, device.Temperature },
                                { States.ONLINE, true }
                            };

                            double temperatureSetpoint;
                            if (device.TemperatureSetback == (double)Temperature.ON)
                            {
                                temperatureSetpoint = 28;
                            }
                            else if (device.TemperatureSetback == (double)Temperature.OFF)
                            {
                                temperatureSetpoint = 8;
                            }
                            else
                            {
                                temperatureSetpoint = device.TemperatureSetback;
                            }

                            responseCommand.States.Add(States.THERMOSTAT_SETPOINT, temperatureSetpoint);

                            string setThermostatMode;
                            if (device.TemperatureSetback == (double)Temperature.OFF)
                            {
                                setThermostatMode = Modes.OFF;
                            }
                            else
                            {
                                setThermostatMode = Modes.ECO;
                            }

                            responseCommand.States.Add(States.THERMOSTAT_MODE, setThermostatMode);

                            // If device is on low battery, add exception code
                            if (device.BatteryLow)
                            {
                                responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                            }

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                    else if (thermostatMode == Modes.OFF)
                    {
                        // If last connection failed, use direct connection
                        ConnectionState connection = await FritzBoxInstance.HandleSetTemperatureAsync(device, (double)Temperature.OFF, customer).ConfigureAwait(false);

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>
                            {
                                { States.THERMOSTAT_TEMPERATURE, device.Temperature },
                                { States.ONLINE, true }
                            };

                            responseCommand.States.Add(States.THERMOSTAT_SETPOINT, (double)Temperature.MINIMUM_VALUE);

                            responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.OFF);

                            // If device is on low battery, add exception code
                            if (device.BatteryLow)
                            {
                                responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                            }

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                    else
                    {
                        // Unsupported mode
                        responseCommand.Status = Status.ERROR;
                        responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                        responseCommand.DebugString = "Unsupported mode";
                        responseCommandList.Add(responseCommand);

                        Logger.Instance.Debug("Unsupported mode");
                    }
                }

                return responseCommandList;
            }
            // If list contains multiple commands, 
            else
            {
                Dictionary<string, object> fritzCommands = new Dictionary<string, object>();

                // Create command list for FRITZ!Box
                foreach (Tuple<ResponseCommand, Execution, Device> command in commandList)
                {
                    // Get data
                    Execution execution = command.Item2;
                    Device device = command.Item3;

                    if (execution.Command == Commands.ONOFF)
                    {
                        fritzCommands[device.Identifier] = (bool)execution.Params[Params.ON];
                    }
                    else if (execution.Command == Commands.ACTIVATE_SCENE)
                    {
                        fritzCommands[device.Identifier] = (bool)execution.Params[Params.DEACTIVATE];
                    }
                    else if (execution.Command == Commands.THERMOSTAT_SETMODE)
                    {
                        // Get requested mode
                        string thermostatMode = execution.Params[Params.THERMOSTAT_MODE].ToString();

                        if (thermostatMode == Modes.ON || thermostatMode == Modes.HEAT)
                        {
                            fritzCommands[device.Identifier] = device.TemperatureComfort;
                        }
                        else if (thermostatMode == Modes.COOL || thermostatMode == Modes.ECO)
                        {
                            fritzCommands[device.Identifier] = device.TemperatureSetback;
                        }
                        else if (thermostatMode == Modes.OFF)
                        {
                            fritzCommands[device.Identifier] = (double)Temperature.OFF;
                        }
                    }
                    else if (execution.Command == Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                    {
                        fritzCommands[device.Identifier] = (double)execution.Params[Params.TEMPERATURE_SETPOINT];
                    }
                }

                // Call FRITZ!Box to handle multiple commands
                Dictionary<string, ConnectionState> commandResponse = await FritzBoxInstance.HandleMultipleCommandsAsync(fritzCommands, customer).ConfigureAwait(false);

                // Generate responses
                foreach (Tuple<ResponseCommand, Execution, Device> command in commandList)
                {
                    // Get data
                    ResponseCommand responseCommand = command.Item1;
                    Execution execution = command.Item2;
                    Device device = command.Item3;

                    // Handle OnOff command
                    if (execution.Command == Commands.ONOFF)
                    {
                        // Grab connection state for this device from FRITZ!Box response list
                        ConnectionState connection;
                        if (commandResponse.ContainsKey(device.Identifier))
                        {
                            connection = commandResponse[device.Identifier];
                        }
                        else
                        {
                            connection = ConnectionState.UNKNOWN;
                        }

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>
                        {
                            { States.ON, (bool)execution.Params[Params.ON] },
                            { States.ONLINE, true }
                        };
                            // If device is on low battery, add exception code
                            if (device.BatteryLow)
                            {
                                responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                            }

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                    // Handle ActivateScene command
                    if (execution.Command == Commands.ACTIVATE_SCENE)
                    {
                        // Grab connection state for this device from FRITZ!Box response list
                        ConnectionState connection;
                        if (commandResponse.ContainsKey(device.Identifier))
                        {
                            connection = commandResponse[device.Identifier];
                        }
                        else
                        {
                            connection = ConnectionState.UNKNOWN;
                        }

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>();

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                    // Handle setmode command
                    else if (execution.Command == Commands.THERMOSTAT_SETMODE)
                    {
                        // Grab connection state for this device from FRITZ!Box response list
                        ConnectionState connection;
                        if (commandResponse.ContainsKey(device.Identifier))
                        {
                            connection = commandResponse[device.Identifier];
                        }
                        else
                        {
                            connection = ConnectionState.UNKNOWN;
                        }

                        // Get requested mode
                        string thermostatMode = execution.Params[Params.THERMOSTAT_MODE].ToString();

                        if (thermostatMode == Modes.ON || thermostatMode == Modes.HEAT)
                        {
                            if (connection == ConnectionState.OK)
                            {
                                // Add response SUCCESS
                                responseCommand.Status = Status.SUCCESS;

                                // Get ambient temperature value
                                // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                                double ambientTemperature = 0;
                                if (device.IsGroup)
                                {
                                    if (device.GroupMembers.Count > 0)
                                    {
                                        Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                                        if (firstGroupDevice != null)
                                        {
                                            ambientTemperature = firstGroupDevice.Temperature;
                                        }
                                        else
                                        {
                                            ambientTemperature = device.TemperatureActualValue;
                                        }
                                    }
                                    else
                                    {
                                        ambientTemperature = device.TemperatureActualValue;
                                    }
                                }
                                else
                                {
                                    ambientTemperature = device.Temperature;
                                }

                                // Add new device state
                                responseCommand.States = new Dictionary<string, object>
                                {
                                    { States.THERMOSTAT_TEMPERATURE, ambientTemperature },
                                    { States.ONLINE, true }
                                };

                                double temperatureSetpoint;
                                if (device.TemperatureComfort == (double)Temperature.ON)
                                {
                                    temperatureSetpoint = 28;
                                }
                                else if (device.TemperatureComfort == (double)Temperature.OFF)
                                {
                                    temperatureSetpoint = 8;
                                }
                                else
                                {
                                    temperatureSetpoint = device.TemperatureComfort;
                                }

                                responseCommand.States.Add(States.THERMOSTAT_SETPOINT, temperatureSetpoint);

                                string setThermostatMode;
                                if (device.TemperatureComfort == (double)Temperature.OFF)
                                {
                                    setThermostatMode = Modes.OFF;
                                }
                                else
                                {
                                    setThermostatMode = Modes.HEAT;
                                }

                                responseCommand.States.Add(States.THERMOSTAT_MODE, setThermostatMode);

                                // If device is on low battery, add exception code
                                if (device.BatteryLow)
                                {
                                    responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                                }

                                // Add command to return list
                                responseCommandList.Add(responseCommand);
                            }
                            else if (connection == ConnectionState.FAILED)
                            {
                                // Device is unreachable
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Device is unreachable";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Device is unreachable");
                            }
                            else
                            {
                                // Connection failed
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Connection failed";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Connection failed");
                            }
                        }
                        else if (thermostatMode == Modes.COOL || thermostatMode == Modes.ECO)
                        {
                            if (connection == ConnectionState.OK)
                            {
                                // Add response SUCCESS
                                responseCommand.Status = Status.SUCCESS;

                                // Get ambient temperature value
                                // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                                double ambientTemperature = 0;
                                if (device.IsGroup)
                                {
                                    if (device.GroupMembers.Count > 0)
                                    {
                                        Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                                        if (firstGroupDevice != null)
                                        {
                                            ambientTemperature = firstGroupDevice.Temperature;
                                        }
                                        else
                                        {
                                            ambientTemperature = device.TemperatureActualValue;
                                        }
                                    }
                                    else
                                    {
                                        ambientTemperature = device.TemperatureActualValue;
                                    }
                                }
                                else
                                {
                                    ambientTemperature = device.Temperature;
                                }

                                // Add new device state
                                responseCommand.States = new Dictionary<string, object>
                                {
                                    { States.THERMOSTAT_TEMPERATURE, ambientTemperature },
                                    { States.ONLINE, true }
                                };

                                double temperatureSetpoint;
                                if (device.TemperatureSetback == (double)Temperature.ON)
                                {
                                    temperatureSetpoint = 28;
                                }
                                else if (device.TemperatureSetback == (double)Temperature.OFF)
                                {
                                    temperatureSetpoint = 8;
                                }
                                else
                                {
                                    temperatureSetpoint = device.TemperatureSetback;
                                }

                                responseCommand.States.Add(States.THERMOSTAT_SETPOINT, temperatureSetpoint);

                                string setThermostatMode;
                                if (device.TemperatureSetback == (double)Temperature.OFF)
                                {
                                    setThermostatMode = Modes.OFF;
                                }
                                else
                                {
                                    setThermostatMode = Modes.ECO;
                                }

                                responseCommand.States.Add(States.THERMOSTAT_MODE, setThermostatMode);

                                // If device is on low battery, add exception code
                                if (device.BatteryLow)
                                {
                                    responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                                }

                                // Add command to return list
                                responseCommandList.Add(responseCommand);
                            }
                            else if (connection == ConnectionState.FAILED)
                            {
                                // Device is unreachable
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Device is unreachable";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Device is unreachable");
                            }
                            else
                            {
                                // Connection failed
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Connection failed";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Connection failed");
                            }
                        }
                        else if (thermostatMode == Modes.OFF)
                        {
                            if (connection == ConnectionState.OK)
                            {
                                // Add response SUCCESS
                                responseCommand.Status = Status.SUCCESS;

                                // Get ambient temperature value
                                // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                                double ambientTemperature = 0;
                                if (device.IsGroup)
                                {
                                    if (device.GroupMembers.Count > 0)
                                    {
                                        Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                                        if (firstGroupDevice != null)
                                        {
                                            ambientTemperature = firstGroupDevice.Temperature;
                                        }
                                        else
                                        {
                                            ambientTemperature = device.TemperatureActualValue;
                                        }
                                    }
                                    else
                                    {
                                        ambientTemperature = device.TemperatureActualValue;
                                    }
                                }
                                else
                                {
                                    ambientTemperature = device.Temperature;
                                }

                                // Add new device state
                                responseCommand.States = new Dictionary<string, object>
                                {
                                    { States.THERMOSTAT_TEMPERATURE, ambientTemperature },
                                    { States.ONLINE, true }
                                };

                                responseCommand.States.Add(States.THERMOSTAT_SETPOINT, (double)Temperature.MINIMUM_VALUE);

                                responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.OFF);

                                // If device is on low battery, add exception code
                                if (device.BatteryLow)
                                {
                                    responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                                }

                                // Add command to return list
                                responseCommandList.Add(responseCommand);
                            }
                            else if (connection == ConnectionState.FAILED)
                            {
                                // Device is unreachable
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Device is unreachable";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Device is unreachable");
                            }
                            else
                            {
                                // Connection failed
                                responseCommand.Status = Status.OFFLINE;
                                responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                                responseCommand.DebugString = "Connection failed";
                                responseCommandList.Add(responseCommand);

                                Logger.Instance.Debug("Connection failed");
                            }
                        }
                        else
                        {
                            // Unsupported mode
                            responseCommand.Status = Status.ERROR;
                            responseCommand.ErrorCode = ErrorCodes.NOTSUPPORTED;
                            responseCommand.DebugString = "Unsupported mode";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Unsupported mode");
                        }
                    }
                    // Handle setpoint command
                    else if (execution.Command == Commands.THERMOSTAT_TEMPERATURE_SETPOINT)
                    {
                        // Grab connection state for this device from FRITZ!Box response list
                        ConnectionState connection;
                        if (commandResponse.ContainsKey(device.Identifier))
                        {
                            connection = commandResponse[device.Identifier];
                        }
                        else
                        {
                            connection = ConnectionState.UNKNOWN;
                        }

                        if (connection == ConnectionState.OK)
                        {
                            // Add response SUCCESS
                            responseCommand.Status = Status.SUCCESS;

                            // Get ambient temperature value
                            // For groups use first group element, as FRITZ!Box is not sending temperature responses for groups (depending on firmware?)
                            double ambientTemperature = 0;
                            if (device.IsGroup)
                            {
                                if (device.GroupMembers.Count > 0)
                                {
                                    Device firstGroupDevice = customer.DeviceList.Find(listedDevice => listedDevice.Id == device.GroupMembers[0]);
                                    if (firstGroupDevice != null)
                                    {
                                        ambientTemperature = firstGroupDevice.Temperature;
                                    }
                                    else
                                    {
                                        ambientTemperature = device.TemperatureActualValue;
                                    }
                                }
                                else
                                {
                                    ambientTemperature = device.TemperatureActualValue;
                                }
                            }
                            else
                            {
                                ambientTemperature = device.Temperature;
                            }

                            // Add new device state
                            responseCommand.States = new Dictionary<string, object>
                            {
                                { States.THERMOSTAT_SETPOINT, (double)execution.Params[Params.TEMPERATURE_SETPOINT] },
                                { States.THERMOSTAT_TEMPERATURE, ambientTemperature },
                                { States.ONLINE, true }
                            };

                            if ((double)execution.Params[Params.TEMPERATURE_SETPOINT] <= device.TemperatureSetback
                                    && device.TemperatureSetback != (double)Temperature.OFF
                                    && device.TemperatureSetback != (double)Temperature.ON)
                            {
                                responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.ECO);
                            }
                            else
                            {
                                responseCommand.States.Add(States.THERMOSTAT_MODE, Modes.HEAT);
                            }

                            // If device is on low battery, add exception code
                            if (device.BatteryLow)
                            {
                                responseCommand.States.Add(States.EXCEPTIONCODE, ErrorCodes.LOWBATTERY);
                            }

                            // Add command to return list
                            responseCommandList.Add(responseCommand);
                        }
                        else if (connection == ConnectionState.FAILED)
                        {
                            // Device is unreachable
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Device is unreachable";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Device is unreachable");
                        }
                        else
                        {
                            // Connection failed
                            responseCommand.Status = Status.OFFLINE;
                            responseCommand.ErrorCode = ErrorCodes.DEVICEOFFLINE;
                            responseCommand.DebugString = "Connection failed";
                            responseCommandList.Add(responseCommand);

                            Logger.Instance.Debug("Connection failed");
                        }
                    }
                }
            }

            return responseCommandList;
        }
    }
}