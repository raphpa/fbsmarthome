﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.JsonObjects;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.Payloads
{
    /// <summary>
    /// Discoverypayload for response event
    /// </summary>
    public class DeleteReportPayload : Payload
    {
        /// <summary>
        /// Discovered Endpoints
        /// </summary>
        [JsonProperty("endpoints")]
        [JsonRequired]
        public List<DeleteEndpoint> Endpoints { get; set; }

        /// <summary>
        /// Scope
        /// </summary>
        [JsonProperty("scope")]
        [JsonRequired]
        public Scope Scope { get; set; }

        /// <summary>
        /// Standartconstructor
        /// </summary>
        public DeleteReportPayload()
        {
            Endpoints = new List<DeleteEndpoint>();
        }
    }
}
