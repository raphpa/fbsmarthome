﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// When you provide a discovery response, you should include a configuration object 
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// True to indicate that you can send this endpoint schedule objects in a control request; otherwise false.
        /// </summary>
        [JsonProperty("supportsScheduling", NullValueHandling = NullValueHandling.Ignore)]
        public bool SupportsScheduling { get; set; }

        /// <summary>
        /// Specifies the temperature modes supported for this endpoint.
        /// </summary>
        [JsonProperty("supportedModes", NullValueHandling = NullValueHandling.Ignore)]
        public List<ThermostatModes> SupportedModes { get; set; }

        /// <summary>
        /// Basicconstructor
        /// </summary>
        public Configuration()
        {
        }
    }
}
