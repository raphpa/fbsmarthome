﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RKon.Alexa.NET46.Types;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// Describes the actions an endpoint is capable of performing and the properties that can be retrieved and report change notifications.
    /// </summary>
    public class Capability
    {
        /// <summary>
        /// Indicates the type of capability, which determines what fields the capability has.
        /// </summary>
        [JsonProperty("type")]
        [JsonRequired]
        [JsonConverter(typeof(StringEnumConverter))]
        public CapabilitiyTypes Type { get; set; }

        /// <summary>
        /// The qualified name of the interface that describes the actions for the device.
        /// </summary>
        [JsonProperty("interface")]
        [JsonRequired]
        public string Interface { get; set; }

        /// <summary>
        /// Indicates the interface version that this endpoint supports.
        /// </summary>
        [JsonProperty("version")]
        [JsonRequired]
        public string Version { get; set; }

        /// <summary>
        /// Indicates if the interface supports deactivation (scene controllers)
        /// </summary>
        [JsonProperty("supportsDeactivation", NullValueHandling = NullValueHandling.Ignore)]
        public bool? SupportsDeactivation { get; set; }

        /// <summary>
        /// Indicates the properties of the interface which are supported by this endpoint in the format "name":"propertyName". If you do not specify a reportable property of the interface in this array, the default is to assume that proactivelyReported and retrievable for that property are false.
        /// </summary>
        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public Properties Properties { get; set; }

        /// <summary>
        /// When you provide a discovery response, you should include a configuration object 
        /// </summary>
        [JsonProperty("configuration", NullValueHandling = NullValueHandling.Ignore)]
        public Configuration Configuration { get; set; }

        /// <summary>
        /// An array of cameraStream structures that provide information about the stream.
        /// </summary>
        [JsonProperty("cameraStreamConfigurations", NullValueHandling = NullValueHandling.Ignore)]
        public List<CameraStreamConfigurations> CameraStreamConfiguration { get; set; }

        /// <summary>
        /// Standardconstructer. Sets Type to AlexaInterface
        /// </summary>
        public Capability()
        {
            Type = Types.CapabilitiyTypes.AlexaInterface;
        }

        /// <summary>
        /// Contructor to fill a AlexaInterface
        /// </summary>
        /// <param name="interfaceName"></param>
        /// <param name="version"></param>
        /// <param name="propertyNames"></param>
        /// /// <param name="cameraStreamConfiguration"></param>
        public Capability(string interfaceName, string version, List<string> propertyNames, bool? proactivelyReported, bool? retrieveable, List<CameraStreamConfigurations> cameraStreamConfiguration = null)
        {
            Type = Types.CapabilitiyTypes.AlexaInterface;
            Interface = interfaceName;
            Version = version;
            if (propertyNames?.Count > 0)
            {
                Properties = new Properties(proactivelyReported, retrieveable);
                foreach (string propertyName in propertyNames)
                    Properties.Supported.Add(new Supported(propertyName));
            }

            CameraStreamConfiguration = cameraStreamConfiguration;
        }

        public Capability(string interfaceName, string version, bool? supportsDeactivation)
        {
            Type = Types.CapabilitiyTypes.AlexaInterface;
            Interface = interfaceName;
            Version = version;
            SupportsDeactivation = supportsDeactivation;
        }
    }
}