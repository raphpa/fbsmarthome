﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RKon.Alexa.NET46.Types;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// Information about the methods that the device uses to connect to the internet and smart home hubs.
    /// </summary>
    public class EndpointConnection
    {
        /// <summary>
        /// The type of connection.
        /// </summary>
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public ConnectionType Type { get; set; }

        /// <summary>
        /// The unique identifier for the network interface controller (NIC). Optionally include this field when the connection type is TCP_IP or ZIGBEE.
        /// </summary>
        [JsonProperty("macAddress", NullValueHandling = NullValueHandling.Ignore)]
        public string MacAddress { get; set; }

        /// <summary>
        /// The Home ID for a Z-Wave network that the endpoint connects to. The format is 0x00000000 with UTF-8 characters. Optionally include this field when the connection type is ZWAVE.
        /// </summary>
        [JsonProperty("homeId", NullValueHandling = NullValueHandling.Ignore)]
        public string HomeId { get; set; }

        /// <summary>
        /// The Node ID for the endpoint in a Z-Wave network that the endpoint connects to. The format is 0x00 with UTF-8 characters. Optionally include this field when the connection type is ZWAVE.
        /// </summary>
        [JsonProperty("nodeId", NullValueHandling = NullValueHandling.Ignore)]
        public string NodeId { get; set; }

        /// <summary>
        /// The connection information for a connection when you can't identify the type of the connection more specifically. The information that you provide in this field should be stable and specific. 
        /// This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public EndpointConnection(ConnectionType type, string value)
        {
            Type = type;
            Value = value;
        }
    }
}