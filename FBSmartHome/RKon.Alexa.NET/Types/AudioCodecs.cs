﻿namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// The audio code for the stream
    /// </summary>
    public enum AudioCodecs
    {
        G711,

        AAC,

        NONE
    }
}
