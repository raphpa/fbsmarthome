﻿namespace FBSmartHome
{
    using Amazon;
    using Amazon.SimpleNotificationService;
    using Amazon.SimpleNotificationService.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public sealed class FritzBoxViaSNS : IFritzBox
    {
        public static readonly FritzBoxViaSNS Instance = new FritzBoxViaSNS();

        private readonly AmazonSimpleNotificationServiceClient Client;

        static FritzBoxViaSNS()
        {
        }

        private FritzBoxViaSNS()
        {
            AmazonSimpleNotificationServiceConfig clientConfig = new AmazonSimpleNotificationServiceConfig
            {
                RegionEndpoint = RegionEndpoint.GetBySystemName(Environment.GetEnvironmentVariable("SNS_REGION"))
            };
            Client = new AmazonSimpleNotificationServiceClient(clientConfig);
        }

        public async Task<ConnectionState> GetDevicesAsync(Customer customer, Connection connection, bool saveCustomer)
        {
            Logger.Instance.Debug("Time: {0}", connection.LastUpdatedDevices.ToString());
            // Only update every 20s, Alexa sometimes requesting discovery way too often
            if (connection.LastUpdatedDevices.AddSeconds(20) > DateTime.UtcNow)
            {
                // else return last connection state
                return connection.LastConnectionState;
            }

            // set update time
            connection.LastUpdatedDevices = DateTime.UtcNow;

            Logger.Instance.Debug("Trying to send SNS message: Get Device List");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);
            MessageAttributeValue attributeConnection = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(connection, new StringEnumConverter())
            };
            messageAttributes.Add("connection", attributeConnection);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "GetDeviceList",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                return ConnectionState.FAILED;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:getdevices",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

            return connection.LastConnectionState;
        }

        public async Task<ConnectionState> HandleSetTemperatureAsync(Device device, double temperature, Customer customer, bool noUpdate = false)
        {
            Logger.Instance.Debug("Trying to send SNS message: Set Temperature");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);

            // Serialize device
            MessageAttributeValue attributeDevice = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(device, new StringEnumConverter())
            };
            messageAttributes.Add("device", attributeDevice);

            // Serialize temperature
            MessageAttributeValue attributeTemperature = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = temperature.ToString()
            };
            messageAttributes.Add("temperature", attributeTemperature);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "SetTemperature",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                return ConnectionState.FAILED;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:settemperature,device:settemperature",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleTurnOffAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Logger.Instance.Debug("Trying to send SNS message: Turn Off Device");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);

            // Serialize device
            MessageAttributeValue attributeDevice = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(device, new StringEnumConverter())
            };
            messageAttributes.Add("device", attributeDevice);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "TurnOffDevice",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                return ConnectionState.FAILED;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnoff,device:turnoff",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleTurnOnAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Logger.Instance.Debug("Trying to send SNS message: Turn On Device");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);

            // Serialize device
            MessageAttributeValue attributeDevice = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(device, new StringEnumConverter())
            };
            messageAttributes.Add("device", attributeDevice);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "TurnOnDevice",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                return ConnectionState.FAILED;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnon,device:turnon",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleApplyTemplateAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Logger.Instance.Debug("Trying to send SNS message: Apply Template");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);

            // Serialize device
            MessageAttributeValue attributeDevice = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(device, new StringEnumConverter())
            };
            messageAttributes.Add("device", attributeDevice);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "ApplyTemplate",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                return ConnectionState.FAILED;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:applytemplate,device:applytemplate",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        // Handle multiple requests
        public async Task<Dictionary<string, ConnectionState>> HandleMultipleCommandsAsync(Dictionary<string, object> commandList, Customer customer)
        {
            Logger.Instance.Debug("Trying to send SNS message: Multiple Commands");
            Dictionary<string, MessageAttributeValue> messageAttributes = new Dictionary<string, MessageAttributeValue>();

            // Serialize customer
            MessageAttributeValue attributeCustomer = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(customer, new StringEnumConverter())
            };
            messageAttributes.Add("customer", attributeCustomer);

            // Serialize command list
            MessageAttributeValue attributeList = new MessageAttributeValue
            {
                DataType = "String",
                StringValue = JsonConvert.SerializeObject(commandList, new StringEnumConverter())
            };
            messageAttributes.Add("commandList", attributeList);

            PublishRequest publishRequest = new PublishRequest
            {
                Message = "MultipleCommands",
                MessageAttributes = messageAttributes,
                TopicArn = Environment.GetEnvironmentVariable("SNS_TOPIC")
            };

            ConnectionState connection = ConnectionState.OK;

            // Publishing to topic
            PublishResponse response;
            try
            {
                Logger.Instance.Debug("Publishing: {0}", JsonConvert.SerializeObject(publishRequest, Formatting.Indented));
                response = await Client.PublishAsync(publishRequest).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Exception on publishing SNS: {0}, {1}", ex.Message, ex.InnerException?.Message);
                connection = ConnectionState.FAILED;
                response = null;
            }

            Logger.Instance.Debug("Publish response: {0}", response?.HttpStatusCode);

            // Add custom metric for datadog logging
            Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:multiplecommands",
                DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(ConnectionNumber.DEFAULT).UseIPv6);

            // Create response list
            Dictionary<string, ConnectionState> responseList = new Dictionary<string, ConnectionState>();

            foreach (KeyValuePair<string, object> command in commandList)
                responseList.Add(command.Key, connection);

            return responseList;
        }
    }
}