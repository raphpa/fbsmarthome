﻿namespace FBSmartHome
{
    using System;

    public sealed class Logger
    {
        public static readonly Logger Instance = new Logger();
        public bool _debugEnabled;

        static Logger()
        {
        }

        private Logger()
        {
            // Enable debug logging if DEBUG is set
            if (Environment.GetEnvironmentVariable("DEBUG") == "true")
            {
                _debugEnabled = true;
            }
        }

        // Logging to console (CloudWatch)
        public void Log(string format, params object[] list)
        {
            string output = string.Format(format, list);
            Console.WriteLine(output);
        }

        // Debug logging to console (CloudWatch) if DEBUG is activated
        public void Debug(string format, params object[] list)
        {
            // If debug is not set, do not write to console
            if (!_debugEnabled)
            {
                return;
            }

            string output = string.Format(format, list);
            Console.WriteLine(output);
        }
    }
}
